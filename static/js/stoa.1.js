$(document).ready(function(){
        
    i18n.setLanguage($.cookie($userInfomation.user_email + "translate"));
    
    
    
    
    //ddslick 플러그인: select box 이쁘게~~
    $('#selectLanguageFrom').ddslick({
        width: 120,
        onSelected: function(selectedData){
            //callback function: do something with selectedData;
        }   
    });
    $('#selectLanguageTo').ddslick({
        width: 120,
        onSelected: function(selectedData){
            //callback function: do something with selectedData;
        }   
    });
    
    
    //request textarea 키입력될 때마다 글자 수 내용 변경해줌
    $("#content").on('keyup keydown change',function(){
        $(".characterCount").text($(this).val().length + " / 140");
        if($(this).val().length>140) $(".characterCount").css("color","red");
        else $(".characterCount").css("color","black");
    });
    
    $("#content").keydown();
    
    $("#frmRequest").submit(function(e){
        e.preventDefault();
        startLoading();
        
        //140자 이상 입력했을 시 request로 전달하기 위해 localStorage에 저장
        if($("#content").val().length>140){
            localStorage.setItem("content",$("#content").val());
            localStorage.setItem("originalLang",$("#selectLanguageFrom").data('ddslick').selectedIndex);
            localStorage.setItem("targetLang",$("#selectLanguageTo").data('ddslick').selectedIndex);
            location.replace("/request");
            return;
        }
        
        
        $("#request_clientId").val($userInfomation.user_email);
        
        var form = $(this)[0];
        var formData = new FormData(form);
        
        $.ajax({
            url: '/api/requests',
            data: formData,
            processData: false,
            contentType: false,
            dataType: "JSON",
            type: 'POST',
            success: function(data){
                endLoading();
                location.href = "/stoa";
            },
            error: function(xhr, ajaxOptions, thrownError){
                // if (xhr.status == 417) {
                //     alert("올바르지 않은 이메일 주소입니다.");
                // }
                // else if(xhr.status == 412){
                //     alert("이미 존재하는 이메일 주소입니다.");
                // }
                alert("작성 에러!");
                endLoading();
            }
        });
    });
    
    
    getRequestsList(1);
    //$("#selectLanguageFrom")
});

function utcToLocal(utc){
    var date = new Date(utc);
    return date;
}

function getRequestsList(t){
    $.ajax({
        url: $apiURL + '/stoa?page=1',
        processData: false,
        contentType: false,
        cache : false,
        dataType: "JSON",
        type: 'GET',
        error: function(xhr, ajaxOptions, thrownError){
            // if (xhr.status == 417) {
            //     alert("올바르지 않은 이메일 주소입니다.");
            // }
            // else if(xhr.status == 412){
            //     alert("이미 존재하는 이메일 주소입니다.");
            // }
            // alert("에러!");
        }
    }).done(function(data){
        
        var miliseconds = 1;
        var seconds = miliseconds * 1000;
        var minutes = seconds * 60;
        var hours = minutes * 60;
        var days = hours * 24;
        var years = days * 365;
        
        
        $(data.data).each(function(i, item) {
            
            
            
            var diff = item.request_dueTime-(new Date()).getTime();
            
            var numYears = Math.floor(diff / years);
            var numDays = Math.floor((diff % years) / days);
            var numHours = Math.floor((diff % days) / hours);
            var numMinutes = Math.floor((diff % hours) / minutes);
            var numSeconds = Math.round((diff % minutes) / seconds);
            
            var remainedTime = "";
            
            if(numYears > 0){
                remainedTime = numYears + "년";
            }
            else{
                if(numDays > 10){
                    remainedTime = numDays + "일";
                }
                else if(numDays > 0){
                    remainedTime = numDays + "일 " + numHours + "시간";
                }
                else{
                    if(numHours > 0){
                        remainedTime = numHours + "시간 " + numMinutes + "분";
                    }
                    else{
                        remainedTime = numMinutes + "분";

                    }
                }
                
            }
            
            
            $("#tickets").append('<div class="ticket" id="ticket' + item.request_id + '">' +
                '<div class="ticket_header">' +
                '<div class="ticket_221_2">' +
                '<span class="ticket_name">'+item.request_clientName+'</span>' +
                '</div>' +
                '<div class="ticket_221_2 invisible1024">' +
                '<span class="subject" style="padding-top: 10px; margin: 0px;">context</span>' +
                '</div>' +
                '<div class="ticket_221_1 right">' +
                '<div class="ticket_type">'+( item.request_isSos ? i18n.getI18n('status_sos') : '$' + parseFloat(item.request_points).toFixed(2) )+'</div>' +
                '</div>' +
                '</div>' +
                '<hr class="invisible1024"/>' +
                '<div class="ticket_content">' +
                '<div class="ticket_221_2">' +
                '<div class="ticket_profile" style="float: left;">' +
                '<img src="/api/access_file/'+item.request_clientPicPath+'"/><br />' +
                '<div class="ticket_timeRemaining">'+ (item.request_status!=-1?('<p i18n="status_time_remaining"></p><p>'+remainedTime +'</p>'):'') + '</div></div>' +
                '<div class="ticket_infomation">' +
                '<div style="width: 100%; background-color:#F5F4F3;">' +
                '<div class="ticket_language">' +
                '<img src="/static/img/flags/round/'+item.request_originalLang+'.png" style="height: 30px; border-radius: 50%;" />' +
                '<p style="font-size: 8px; padding: 0px;">'+i18n.getI18n("lang_" + item.request_originalLang)+'</p>' +
                '</div>' +
                '<div style="float: left; width: 10px; padding-top: 20px;">' +
                '<img src="/static/img/flags/round/arrow.png" style="width: 10px; border-radius: 50%;" />' +
                '</div>' +
                '<div class="ticket_language">' +
                '<img src="/static/img/flags/round/'+item.request_targetLang+'.png" style="height: 30px; border-radius: 50%;" />' +
                '<p style="font-size: 8px; padding: 0px;">'+i18n.getI18n("lang_" + item.request_targetLang)+'</p>' +
                '</div>' +
                '</div>' +
                '<!--<div style="width: 100%; background-color: transparent;"><div class="ticket_contentType"><img src="/static/img/flags/round/arrow.png" style="height: 30px; border-radius: 50%;" /><p style="font-size: 8px; padding: 0px;">출판물</p></div><div style="float: left; width: 10px; padding-top: 20px;"></div><div class="ticket_contentType"><img src="/static/img/flags/round/arrow.png" style="height: 30px; border-radius: 50%;" /><p style="font-size: 8px; padding: 0px;">과학</p></div></div>-->' +
                '<p class="ticket_wordCount">'+item.request_words+' 단어</p>' +
                '</div>' +
                '</div>' +
                '<div class="ticket_221_2">' +
                '<div class="ticket_contentTextWrap">' +
                '<span class="ticket_contentText">'+item.request_context+'</span>' +
                '</div>' +
                '</div>' +
                '<div class="ticket_221_1">' +
                '<div class="ticket_button translator_only tryTranslate">' +
                '번역하기'+
                '</div>' +
                '</div>' +
                '<!--<div class="ticket_221_1 right" style="position: absolute; bottom: 0px; right: 0px;">'+(new Date(item.request_registeredTime)).toLocaleString()+'</div>-->' +
                '</div>');
                
            $('.ticket_contentTextWrap').off('click').on('click', function(){
                
                if($(this).css('height') != '135px'){
                    $(this).css('height','135px');
                }
                else{
                    $('.ticket_contentTextWrap').css('height','135px');
                    $(this).css('height','auto');
                }
                
            });
            
            if(item.request_translatorName){
                $('#ticket' + item.request_id).prepend('<div class="ticket_alert"><div class="ticket_alert_background"></div><div class="ticket_alert_inner"><span>'+item.request_translatorName+'님이 번역 중</span><!--<button type="button" class="positive request_detail">내용 확인 / 재등록</button><button type="button" class="negative request_delete" value="'+item.request_id+'">삭제하기</button>--></div></div>');
            }
            
            
            $('#ticket' + item.request_id + ' .tryTranslate').click(function(){
                        
                $("body").prepend('<div class="popup" id="popup'+item.request_id+'"><div class="popup_background"></div><div class="dialog" style="display: none;"><div class="dialog_image"><img src="/static/img/9.7.1feedback3_web.png" /></div><span class="dialog_message">번역을 시작할까요?</span><div class="dialog_buttons"><button type="button" class="positive">아니오</button><button type="button" class="negative">예</button></div></div></div>');
                // $("#popup"+item.request_id).delay(1).queue(function(){$("#popup"+item.request_id).removeClass("invisible");});
                $("#popup"+item.request_id + " .dialog").fadeIn(300);
                
                
                var formData = new FormData();
                formData.append('request_id', item.request_id);
                
                
                
                $("#popup"+item.request_id).find(".negative").click(function(){
                    $.ajax({
                        url: $apiURL + '/ongoing',
                        processData: false,
                        contentType: false,
                        data: formData,
                        cache : false,
                        dataType: "JSON",
                        type: 'POST',
                        error: function(xhr, ajaxOptions, thrownErr7or){
                            // if (xhr.status == 417) {
                            //     alert("올바르지 않은 이메일 주소입니다.");
                            // }
                            // else if(xhr.status == 412){
                            //     alert("이미 존재하는 이메일 주소입니다.");
                            // }
                            alert("에러!\n" + JSON.parse(xhr.responseText).message);
                        }
                    }).done(function(data){
                        alert("번역을 시작합니다.");
                        location.href = '/status';
                    });
                });
                
                $("#popup"+item.request_id).find(".positive").click(function(){
                    $("#popup"+item.request_id).remove();
                });
                
            });
        });
    });
}

    