
$(document).ready(function(){
    
    i18n.setLanguage($.cookie($userInfomation.user_email + "translate"));
    
    $(".notification_close").click(function(){
        $(this).parent().remove();
        if(!$.trim($("#notification").html())){
            $("#notification").remove();
            $("#subjectNotification").remove();
        }
    });
    
    getRequestsList(1);
    
    ///api/user/requests/incomplete
    
    
});



function getRequestsList(t){
    $.ajax({
        url: $apiURL + '/incomplete?page=1',
        processData: false,
        contentType: false,
        cache : false,
        dataType: "JSON",
        type: 'GET',
        error: function(xhr, ajaxOptions, thrownError){
            // if (xhr.status == 417) {
            //     alert("올바르지 않은 이메일 주소입니다.");
            // }
            // else if(xhr.status == 412){
            //     alert("이미 존재하는 이메일 주소입니다.");
            // }
            // alert("에러!");
        }
    }).done(function(data){
        
        var miliseconds = 1;
        var seconds = miliseconds * 1000;
        var minutes = seconds * 60;
        var hours = minutes * 60;
        var days = hours * 24;
        var years = days * 365;
        
        $(data.data).each(function(i, item) {
            // alert(new Date(item.request_dueTime-(new Date()).getTime()).toLocaleString());
            // alert(new Date(item.request_dueTime).toLocaleString());
            // alert((new Date()).toLocaleString());
            
            
            var diff = item.request_dueTime-(new Date()).getTime();
            
            var numYears = Math.floor(diff / years);
            var numDays = Math.floor((diff % years) / days);
            var numHours = Math.floor((diff % days) / hours);
            var numMinutes = Math.floor((diff % hours) / minutes);
            var numSeconds = Math.round((diff % minutes) / seconds);
            
            var remainedTime = "";
            
            if(numYears > 0){
                remainedTime = numYears + "년";
            }
            else{
                if(numDays > 10){
                    remainedTime = numDays + "일";
                }
                else if(numDays > 0){
                    remainedTime = numDays + "일 " + numHours + "시간";
                }
                else{
                    if(numHours > 0){
                        remainedTime = numHours + "시간 " + numMinutes + "분";
                    }
                    else{
                        remainedTime = numMinutes + "분";

                    }
                }
                
            }
            
            $("#tickets").append('<div class="ticket buttons" id="ticket_'+item.request_id+'"><div class="ticket_header"><div class="ticket_221_2"><span class="ticket_name">'+item.request_clientName+'</span></div><div class="ticket_221_2 invisible1024"><span class="subject" style="padding-top: 10px; margin: 0px;">context</span></div><div class="ticket_221_1 right"><div class="ticket_type">'+( item.request_isSos ? i18n.getI18n('status_sos') : '$' + parseFloat(item.request_points).toFixed(2) )+'</div></div></div><hr class="invisible1024"/><div class="ticket_content"><div class="ticket_221_2"><div class="ticket_profile" style="float: left;"><img src="/api/access_file/'+item.request_clientPicPath+'"/><br /><div class="ticket_timeRemaining">'+ (item.request_status!=-1?('<p i18n="status_time_remaining"></p><p>'+remainedTime +'</p>'):'') + '</div></div><div class="ticket_infomation"><div style="width: 100%; background-color:#F5F4F3;"><div class="ticket_language"><img src="/static/img/flags/round/'+item.request_originalLang+'.png" style="height: 30px; border-radius: 50%;" /><p style="font-size: 8px; padding: 0px;">'+i18n.getI18n("lang_" + item.request_originalLang)+'</p></div><div style="float: left; width: 10px; padding-top: 20px;"><img src="/static/img/flags/round/arrow.png" style="width: 10px; border-radius: 50%;" /></div><div class="ticket_language"><img src="/static/img/flags/round/'+item.request_targetLang+'.png" style="height: 30px; border-radius: 50%;" /><p style="font-size: 8px; padding: 0px;" i18n="lang_'+item.request_targetLang +'"></p></div></div>'+(!item.request_isSos?'<div style="width: 100%; background-color: transparent;"><div class="ticket_contentType"><img src="/static/img/format/'+item.request_format+'.png" style="height: 30px; border-radius: 50%;" /><p style="font-size: 8px; padding: 0px;">'+ i18n.getI18n('format_' + item.request_format)+'</p></div><div style="float: left; width: 10px; padding-top: 20px;"></div><div class="ticket_contentType"><img src="/static/img/subject/'+item.request_subject+'.png" style="height: 30px; border-radius: 50%;" /><p style="font-size: 8px; padding: 0px;">'+ i18n.getI18n('subject_' + item.request_subject)+'</p></div></div>':"")+'<p class="ticket_wordCount">'+ i18n.getI18n("status_letters", item) +'</p></div></div><div class="ticket_221_2">'+item.request_context+'</div><!--<div class="ticket_221_1 right" style="position: absolute; bottom: 0px; right: 0px;">'+(new Date(item.request_registeredTime)).toLocaleString()+'</div>--></div></div>');
            
            
            
            
            if(item.request_status==-1){
                $("#tickets").find(".ticket").last().prepend('<div class="ticket_alert"><div class="ticket_alert_background"></div><div class="ticket_alert_inner"><span i18n="status_expired"></span><!--<button type="button" class="positive request_detail">내용 확인 / 재등록</button><button type="button" class="negative request_delete" value="'+item.request_id+'">삭제하기</button>--></div></div>');
                
                
                $(".request_delete").off("click").on("click", function(e){
                    var what = $(this);
                        
                    $("body").prepend('<div class="popup" id="popup'+what.val()+'"><div class="popup_background"></div><div class="dialog" style="display: none;"><div class="dialog_image"><img src="/static/img/9.7.1feedback3_web.png" /></div><span class="dialog_message">정말로 삭제하시겠습니까?</span><div class="dialog_buttons"><button type="button" class="positive">아니오</button><button type="button" class="negative">예</button></div></div></div>');
                    // $("#popup"+what.val()).delay(1).queue(function(){$("#popup"+what.val()).removeClass("invisible");});
                    $("#popup"+what.val() + " .dialog").fadeIn(300);
                    
                    
                    $("#popup"+what.val()).find(".negative").click(function(){
                        $.ajax({
                            url: $apiURL + '/incomplete/' + what.val(),
                            processData: false,
                            contentType: false,
                            cache : false,
                            dataType: "JSON",
                            type: 'DELETE',
                            error: function(xhr, ajaxOptions, thrownErr7or){
                                // if (xhr.status == 417) {
                                //     alert("올바르지 않은 이메일 주소입니다.");
                                // }
                                // else if(xhr.status == 412){
                                //     alert("이미 존재하는 이메일 주소입니다.");
                                // }
                                // alert("에러!");
                            }
                        }).done(function(data){
                            alert("삭제되었습니다.");
                            $("#ticket_" + what.val()).remove();
                            
                            $("#popup"+what.val()).remove();
                        });
                    });
                    
                    $("#popup"+what.val()).find(".positive").click(function(){
                        $("#popup"+what.val()).remove();
                    });
                    
                    
                });
            }
            else if(item.request_translatorName){
                $("#tickets").find(".ticket").last().prepend('<div class="ticket_alert requester_only"><div class="ticket_alert_background"></div><div class="ticket_alert_inner"><span>'+item.request_translatorName+'님이 번역 중</span><!--<button type="button" class="positive request_detail">내용 확인 / 재등록</button><button type="button" class="negative request_delete" value="'+item.request_id+'">삭제하기</button>--></div></div>');
            }
            
            $("#ticket_"+item.request_id).click(function(){
                if($userInfomation.user_isTranslator && (item.request_status>0)){
                    location.href = '/translate?id=' + item.request_id;
                }
                else{
                    showTicket(item.request_id);
                }
            });            
            $("#ticket_"+item.request_id).find(".request_detail").click(function(){
                
                showTicket(item.request_id);
                // $("body").prepend('<div class="popup invisible" id="popup_'+item.request_id+'"><div class="popup_background"></div><div class="detail"><div class="detail_close"><i class="icon ion-ios-close-empty fa-4x"></i></div><div class="detail_header"> <span class="detail_header_left"> Infomation </span> <span class="detail_header_right"> 2015.02.01 15:03 </span> </div> <hr /> <span class="detail_title">Context</span> <span class="detail_content">'+item.request_context+'</span> <div class="detail_infomations"> <div class="detail_infomation"> <span class="detail_infomation_title">언어</span> <hr /> <div class="detail_infomation_icons"> <div class="detail_infomation_icon"> <img src="/static/img/flags/round/'+item.request_originalLang+'.png" style="height: 30px; border-radius: 50%;" /> <p style="font-size: 8px; padding: 0px;">'+i18n.getI18n("lang_" + item.request_originalLang)+'</p> </div> <div style="float: left; width: 10px; padding-top: 20px;"> <img src="/static/img/flags/round/arrow.png" style="width: 10px; border-radius: 50%;" /> </div> <div class="detail_infomation_icon"> <img src="/static/img/flags/round/'+item.request_targetLang+'.png" style="height: 30px; border-radius: 50%;" /> <p style="font-size: 8px; padding: 0px;">'+i18n.getI18n("lang_" + item.request_targetLang)+'</p> </div> </div> </div> '+(!item.request_isSos? '<div class="detail_infomation"> <span class="detail_infomation_title">포맷</span> <hr /> <div class="detail_infomation_icons"> <div class="detail_infomation_icon"> <img src="/static/img/subject/'+item.request_subject+'.png" style="height: 30px; border-radius: 50%;" /> <p style="font-size: 8px; padding: 0px;">'+ i18n.getI18n('subject_' + item.request_subject)+'</p> </div> <div style="float: left; width: 10px; padding-top: 20px;"> </div> <div class="detail_infomation_icon"> <img src="/static/img/format/'+item.request_format+'.png" style="height: 30px; border-radius: 50%;" /> <p style="font-size: 8px; padding: 0px;">'+ i18n.getI18n('format_' + item.request_format)+'</p> </div> </div> </div>':'<div class="detail_infomation"> <span class="detail_infomation_title">포맷</span> <hr /> <span class="detail_infomation_title">무료단문번역</span> </div>')+' <div class="detail_infomation"> <span class="detail_infomation_title">단어수</span> <hr /> <span class="detail_infomation_words">'+item.request_words+'</span> </div> </div> <span class="detail_title">Original Text</span> <span class="detail_content">'+item.request_text+'</span> </div> </div>');
                
                
                // $("#popup_"+item.request_id).delay(1).queue(function(){$("#popup_"+item.request_id).removeClass("invisible");});
                // $("#popup_"+item.request_id).find(".detail_close i").click(function(){
                //     $("#popup_"+item.request_id).remove();
                // });
            });
        
    
        });
    });
    
}
