
$(document).ready(function(){
    i18n.setLanguage($.cookie($userInfomation.user_email + "translate"));
    $.ajax({
        url: '/api/notification',
        processData: false,
        contentType: false,
        cache : false,
        dataType: 'JSON',
        type: 'GET',
        error: function(){
            //location.href = "/about";
        }
    }).done(function(data){
        $(data.data).each(function(i, item) {
            
            $("#notification").append('<div class="notification2 buttons" id="notification'+item.noti_id+'"> '+(item.is_read?'':'<div class="notification_unread"><i class="fa fa-circle" aria-hidden="true"></i></div>')+' <div class="notification_icon"> <img class="notification_icon_profile" src="api/access_file/'+item.profilePic+'"/> </div> <div class="notification_content"> <span class="notification_time">'+ (new Date(item.ts)).toLocaleString() +'</span> <span class="notification_text">'+i18n.getI18n('notification_' + item.noti_typeId, item)+'</span> </div> </div>');
            
            $("#notification" + item.noti_id).click(function(){
                showTicket(item.request_id);
            });
            // $(".notification_close").bind('click', function(){
            //     $(this).parent().remove();
            //     if(!$.trim($("#notification").html())){
            //         $("#notification").remove();
            //         $("#subjectNotification").remove();
            //     }
            // });
        });
    });
});
