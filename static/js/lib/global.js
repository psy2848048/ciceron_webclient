var $userInfomation;
var $apiURL = "/api/user/requests";
// $(window).on('popstate',function(event) {
//     if(!$("#header-profile").hasClass('invisible')){
//         history.forward();
//         setTimeout(function() {
//             $("#header-profile").addClass('invisible');
//         }, 100);
//     }
//     else{
//         location.reload();
//     }
// });


function $_GET(param) {
    var vars = {};
    window.location.href.replace( 
        /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
        function( m, key, value ) { // callback
            vars[key] = value !== undefined ? value : '';
        }
    );

    if ( param ) {
        return vars[param] ? vars[param] : "";	
    }
    return vars;
}


$(window).on('popstate',function(event) {
    
    if(!$("#header-profile").hasClass('invisible')){
        $("#header-profile").addClass('invisible');
    }
});


$.ajax({
    url: '/api/user/profile',
    processData: false,
    contentType: false,
    cache : false,
    async: false,
    dataType: 'JSON',
    type: 'GET',
    error: function(){
        location.href = "/about";
    }
}).done(function(data){
    
    $userInfomation = data;
    if($userInfomation.user_isTranslator) {
        $apiURL = "/api/user/translations";
        $('body').addClass('translator');
    }
    else{
        $apiURL = "/api/user/requests";
        $('body').addClass('requester');
    }
    $.cookie($userInfomation.user_email + "translate", $.cookie($userInfomation.user_email + "translate") || $userInfomation.user_motherLang);
    // $userInfomation.user_motherLang = $.cookie($userInfomation.user_email + "translate") || $userInfomation.user_motherLang;

    
});
$("#header").load("header.html",function(){
    
    // $('body').on("selectstart", function(event){ return false; });
    // $('body').on("dragstart", function(event){ return false; });
    // $('body').on("contextmenu",function(){ return false; }); 
    // $("body").prepend('<div id="popup" class="invisible"><div id="popup_background"></div></div>');
    
    $(document).bind('mousedown', function(e){
        if(e.target.id != 'img-header' && !$('#header-profile').find(e.target).length){
            if(!$("#header-profile").hasClass('invisible')){
                $("#header-profile").addClass('invisible');
                history.back();
            }
        }
    });
    
    $("a[href|='"+$(location).attr('pathname') +"']").addClass("selected");
    
    $("#img-header").click(function(){
        if($("#header-profile").hasClass('invisible')){
            $("#header-profile-container").css("max-height",$(window).height() - 150);
            $("#header-profile").removeClass('invisible');
            var obj = { Page: "CICERON", Url: location.href };
            history.pushState(obj, obj.Page, obj.Url);
            
        }
        else{
            $("#header-profile").addClass('invisible');
            history.back();
            
        }
    });
    
    $("#btn_logout").click(function(){
        
        $.ajax({
            url: '/api/logout',
            processData: false,
            contentType: false,
            cache : false,
            dataType: 'JSON',
            type: 'GET',
            error: function(){
                alert("잠시 후 시도해주세요.");
            }
        }).done(function(data){
            location.href = "/about";
        });
        
    });
    
    $("#footer").load("footer.html",function(){

        $("#img-header").attr('src', '/api/access_file/' + $userInfomation.user_profilePicPath);
        $("#img-profile").attr('src', '/api/access_file/' + $userInfomation.user_profilePicPath);
        $("#header-profile-name").text($userInfomation.user_name);

        $("#translatePage option:eq("+($.cookie($userInfomation.user_email + "translate")-1) +")").prop("selected", true);
        
        $("#translatePage").change(function(){
            
            $.cookie($userInfomation.user_email + "translate", $(this).find("option:selected").val());
            // $userInfomation.user_motherLang = $(this).find("option:selected").val();
            i18n.setLanguage($(this).find("option:selected").val());
            // alert($.cookie($userInfomation.user_email + "translate"));
            // alert($(this).find("option:selected").val());
        
        });

        $.ajax({
            url: '/api/notification',
            processData: false,
            contentType: false,
            cache : false,
            dataType: 'JSON',
            type: 'GET',
            error: function(){
                //location.href = "/about";
            }
        }).done(function(data){
            if(data.numberOfNoti>0&&$(location).attr('pathname')!="/status"){
                $("#btnStatus").append('<i class="fa fa-circle" style="color: #FE8B97; font-size: 5px; position: absolute;"></i>');
                $("#btnStatus").addClass("shake-slow shake-constant shake-constant--hover");
            }
           
           
            
            $('a').on('click',function(e){
                // e.preventDefault();
                // var obj = { Page: "CICERON", Url: $(this).attr('href') };
                // history.pushState(obj, obj.Page, obj.Url);
                // location.replace(obj.Url);
                // $.ajax({url: obj.Url}).done(function(data){
                //     $('html').html(data);
                // });
                
                // $('html').empty();
                // $('html').append('<iframe style="width: 100%; height: 1000px; border: none;"></iframe>')
                // $('iframe').attr('src',obj.Url);
                
            });

        });
    });
    
    
    
});


var scrollTimer = null;
var functionScroll = function(e){
    var $nowScrollTop = $(this).scrollTop();
    if($nowScrollTop > 10){
        $("#header").addClass('scrolled');
    }
    else{
        $("#header").removeClass('scrolled');                
    }
};


$(this).scroll(function(){
    if (scrollTimer) {
        clearTimeout(scrollTimer);   // clear any previous pending timer
    }
    scrollTimer = setTimeout(functionScroll, 5);   // set new timer
});
$('body').on('touchmove', function () {
    if (scrollTimer) {
        clearTimeout(scrollTimer);   // clear any previous pending timer
    }
    scrollTimer = setTimeout(functionScroll, 5);   // set new timer
});
functionScroll();

$('head').append('<link rel="stylesheet" href="css/csshake.min.css">');




function startLoading(){
    $("body").prepend('<div class="popup" id="popup_loading"><img class="loading" src="/static/img/loading.gif" style="position: absolute; left: 50%; top: 50%; transform: translate(-50%, -50%);"/></div>');
    
}

function endLoading(){
    $("#popup_loading").remove();
}




function showTicket($id){
    
    startLoading();
    
    $.ajax({
        url: $apiURL + '/' + $id,
        processData: false,
        contentType: false,
        cache : false,
        async: true,
        dataType: 'JSON',
        type: 'GET',
        error: function(){
            // location.href = "/about";
            endLoading();
        }
    }).done(function(data){
        
        // alert(data.data[0].request_id);
        $(data.data).each(function(i, item) {
            
            $("body").prepend('<div class="popup" id="popup_' + $id +'"><div class="popup_background"></div></div>');
            $("#popup_" + item.request_id).append('<div class="detail" style="display:none;">' +
                '<div class="detail_close"><i class="icon ion-ios-close-empty fa-4x"></i></div>' + 
                '<div class="detail_header">' +
                '<span class="detail_header_left"> Infomation </span>' +
                '<span class="detail_header_right">' +(new Date(item.request_registeredTime)).toLocaleString() +'<br />~' +(new Date(item.request_dueTime)).toLocaleString() +'</span>' +
                '</div>' + 
                '<hr />' +
                '<div id="detail_context">' +
                '<span class="detail_title">Context</span>' +
                '<span class="detail_content">' +item.request_context+'</span>' +
                '</div>' +
                '<div class="detail_infomations">' +
                '<div class="detail_infomation">' +
                '<span class="detail_infomation_title">언어</span>' +
                '<hr /> <div class="detail_infomation_icons">' +
                '<div class="detail_infomation_icon">' +
                '<img src="/static/img/flags/round/' +item.request_originalLang+'.png" style="height: 30px; border-radius: 50%;" />' +
                '<p style="font-size: 8px; padding: 0px; width: 30px;">' +i18n.getI18n("lang_" + item.request_originalLang) +'</p>' +
                '</div>' +
                '<div style="float: left; width: 10px; padding-top: 20px;">' +
                '<img src="/static/img/flags/round/arrow.png" style="width: 10px; border-radius: 50%;" />' +
                '</div>' +
                '<div class="detail_infomation_icon">' +
                '<img src="/static/img/flags/round/' +item.request_targetLang+'.png" style="height: 30px; border-radius: 50%;" />' +
                '<p style="font-size: 8px; padding: 0px; width: 30px;">' +i18n.getI18n("lang_" + item.request_targetLang) +'</p>' +
                '</div>' +
                '</div>' +
                '</div>' +
                (!item.request_isSos? '<div class="detail_infomation"> <span class="detail_infomation_title">포맷</span> <hr /> <div class="detail_infomation_icons"> <div class="detail_infomation_icon"> <img src="/static/img/format/' +item.request_format +'.png" style="height: 30px; border-radius: 50%;" /> <p style="font-size: 8px; padding: 0px; width: 30px;">' + i18n.getI18n('format_' + item.request_format) +'</p> </div> <div style="float: left; width: 10px; padding-top: 20px;"> </div> <div class="detail_infomation_icon"> <img src="/static/img/subject/' +item.request_subject+'.png" style="height: 30px; border-radius: 50%;" /> <p style="font-size: 8px; padding: 0px; width: 30px;">' + i18n.getI18n('subject_' + item.request_subject) +'</p> </div> </div> </div>':'<div class="detail_infomation"> <span class="detail_infomation_title">포맷</span> <hr /> <span class="detail_infomation_title">' + i18n.getI18n("status_sos") +'</span> </div>') +
                '<div class="detail_infomation">' +
                '<span class="detail_infomation_title">단어수</span>' +
                '<hr /> <span class="detail_infomation_words">' +item.request_words+'</span>' +
                '</div>' +
                '</div>' +
                '<span class="detail_title">Original Text</span>' +
                '<span class="detail_content">' +item.request_text+'</span>' +
                '<div id="detail_translated_text">' +
                '<span class="detail_title">Translated Text</span>' +
                '<span class="detail_content">' +item.request_translatedText+'</span>' +
                '</div>' +
                '</div>');

            if(item.request_isSos){
                $("#detail_context").remove();
            }
            
            if(!item.request_translatedText){
                $("#detail_translated_text").remove();
            }
            
            if(item.request_clientId == $userInfomation.user_email){
                
                if(item.request_status == -1){
                    $('.detail_header_left').html(i18n.getI18n('status_expired'));
                    $('.detail_header_left').css("color", "red");
                }
                
            }
            // $("#popup_"+item.request_id).removeClass("invisible");
            // $("#popup_"+item.request_id).delay(1).queue(function(){$("#popup_"+item.request_id).removeClass("invisible");});
            $("#popup_"+item.request_id + " .detail").fadeIn(300);
            $("#popup_"+item.request_id).find(".detail_close i").click(function(){
                
                $("#popup_"+item.request_id + " .detail").fadeOut(300,function(){$("#popup_"+item.request_id).remove();});
            });
        });
        endLoading();
    });
}