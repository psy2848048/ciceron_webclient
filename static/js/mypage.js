

$(document).ready(function(){
    i18n.setLanguage($.cookie($userInfomation.user_email + "translate"));
    
    
    $(".mypage_image_img").attr('src', '/api/access_file/' + $userInfomation.user_profilePicPath);
    $(".mypage_email").text($userInfomation.user_email);
    $(".mypage_detail_language").text(i18n.getI18n("lang_"+$userInfomation.user_motherLang) );
    $(".mypage_detail_point").text($userInfomation.user_point);
    
    // $(".mypage_detail_language").text(i18n.getI18n("lang_1"));
    
    
    // alert(i18n.getI18n("lang_0"));
    $(".mypage_image_edit").click(function(){
        $("#user_profilePic").click();
    });
    
    
    $(".frmChangePassword").submit(function(e){
        e.preventDefault();
        
        
        if($("#new_password1").val() != $("#new_password2").val()){
            alert("변경 할 두 비밀번호가 서로 일치하지 않습니다.");
            $("#new_password1").select();
            return;
        }
        
        if($("#new_password1").val()==""){
            alert("변경할 비밀번호를 입력해주세요.");
            return;
        }
        
        $("#hidden_old_password").val($.sha256($("#old_password").val()));
        $("#hidden_new_password").val($.sha256($("#new_password1").val()));
        var form = $(this)[0];
        var formData = new FormData(form);
        
        
        
        $.ajax({
            url: '/api/user/change_password',
            data: formData,
            processData: false,
            contentType: false,
            dataType: "JSON",
            type: 'POST',
            error: function(xhr, ajaxOptions, thrownErr7or){
                if (xhr.status == 403) {
                    alert("현재 비밀번호가 올바르지 않습니다.");
                    $("#old_password").select();
                }
                else if(xhr.status == 405){
                    alert("변경할 비밀번호를 입력해주세요.");
                }
                // alert("에러!");
            }
        }).done(function(data){
            alert("정상적으로 변경되었습니다.");
            location.reload();
        });
        
    });
    
    $("#change_password").click(function(){
        if($(window).width() <= 960){
            $('.changePassword').stop().slideToggle();
        }
        else{
            $('.changePassword').stop().toggle();
        }
        
    });
    
    $("#frmUser_profilePic").submit(function(e){
        
        
        e.preventDefault();
        
        // var form = $(this)[0];
        // var formData = new FormData(form);
        
        // $.ajax({
        //     url: '/api/user/profile',
        //     data: formData,
        //     processData: false,
        //     contentType: false,
        //     dataType: "JSON",
        //     type: 'POST',
        //     error: function(xhr, ajaxOptions, thrownErr7or){
        //         // if (xhr.status == 417) {
        //         //     alert("올바르지 않은 이메일 주소입니다.");
        //         // }
        //         // else if(xhr.status == 412){
        //         //     alert("이미 존재하는 이메일 주소입니다.");
        //         // }
        //         // alert("에러!");
        //     }
        // }).done(function(data){
        //     alert("정상적으로 변경되었습니다.");
        // });
        
        
    });
    
    $("#user_profilePic").bind('change',function(e){
        
        
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                
                $("body").prepend('<div class="popup"> <div class="popup_background"></div> <div class="imgSetting"> <div class="imgSetting_close"> <i class="icon ion-ios-close-empty fa-4x"></i> </div> <div class="imgSetting_header"> <span class="imgSetting_header_left"> 프로필 사진 지정 </span> <span class="imgSetting_header_right"> </span> </div> <div class="imgSetting_content"> <div class="imgSetting_cropDiv"> <form method="POST" enctype="multipart/form-data" id="frmImg_crop"> <img class="imgSetting_crop cropImage" /> <span class="imgSetting_cropInfomation"> <i class="fa fa-arrows" aria-hidden="true"></i> 위치를 조정하고 확대할 수 있습니다. </span> <button type="submit" style="width: 100%;">저장</button> </form> </div> </div> </div> </div>');
                
                
                var $croppedImg;
                var $tt;
                $('.imgSetting_crop').attr('src', e.target.result);
                $('.imgSetting_crop').cropbox({width: 300, height: 300, showControls: 'auto' }).on('cropbox', function(event, results, img){
                    $croppedImg = img.getBlob();
                    $tt = img.getDataURL();
                });
                
                $('.imgSetting_close').click(function(){
                    $('.popup').remove();
                });
                $("#frmImg_crop").submit(function(e){
                    e.preventDefault();
                    
                    var formData = new FormData();
                    formData.append("user_profilePic", $croppedImg, "thumb.png");
                    $.ajax({
                        url: '/api/user/profile',
                        data: formData,
                        processData: false,
                        contentType: false,
                        dataType: "JSON",
                        type: 'POST',
                        error: function(xhr, ajaxOptions, thrownErr7or){
                            // if (xhr.status == 417) {
                            //     alert("올바르지 않은 이메일 주소입니다.");
                            // }
                            // else if(xhr.status == 412){
                            //     alert("이미 존재하는 이메일 주소입니다.");
                            // }
                            // alert("에러!");
                        }
                    }).done(function(data){
                        alert("정상적으로 변경되었습니다.");
                        location.reload();
                    });
                });
                
                
                // $("#frmUser_profilePic").submit();
            }

            reader.readAsDataURL(this.files[0]);
        }
        
        
    });
    
    
});

