$(document).ready(function(){
    
    i18n.setLanguage($.cookie($userInfomation.user_email + "translate"));
    
    var $photoText = "";
    

    
    for( i = 0; i < 15; i++){
        $("#selectFormat").append('<option value="' + i + '" data-imagesrc="/static/img/format/' + i +'.png" data-description="">' + i18n.getI18n('format_' + i) + '</option>');
    }
    
    for(i = 0; i < 12; i++){
        $("#selectField").append('<option value="' + i + '" data-imagesrc="/static/img/subject/' + i +'.png" data-description="">' + i18n.getI18n('subject_' + i) + '</option>');
    }
    
    
    $( ".datepicker" ).datepicker({
        inline: true,
        showOtherMonths: true,
        minDate: new Date()
    });
    
    var $preScrollHeight = 0;
    $("#request_text").on('keyup keydown change',function(){
        var $nowScroll = $(document).scrollTop();
        $(this).css('height', 'auto' );
        $(this).height( this.scrollHeight );
        $(document).scrollTop($nowScroll + this.scrollHeight - $preScrollHeight);
        $preScrollHeight = this.scrollHeight;
        
        $(".characterCount").text($(this).val().length + " 글자");
        setPrice();
    });
    
    $("#request_text").keydown();

    $("#priceBar").on('input change', function(){
        $("#nowPrice").val((parseFloat($(this).val())).toFixed(2));
    });
    
    $("#nowPrice").on('input change', function(){
        $("#priceBar").val((parseFloat($(this).val())).toFixed(2));
        
    });
    
    
    $('#selectLanguageFrom').ddslick({
        width: 120,
        onSelected: function(selectedData){
            //callback function: do something with selectedData;
        }   
    });
    $('#selectLanguageTo').ddslick({
        width: 120,
        onSelected: function(selectedData){
            //callback function: do something with selectedData;
        }   
    });
    
    $('#selectFormat').ddslick({
        height: 300,
        onSelected: function(selectedData){
            //callback function: do something with selectedData;
        }   
    });
    
    $('#selectField').ddslick({
        height: 300,
        onSelected: function(selectedData){
            //callback function: do something with selectedData;
        }   
    });
    
    $(".content_select").click(function(){
        $(".content_select").removeClass("selected");
        $(this).addClass("selected");
        
        $(".request_isType").attr("value", "0");
        $("#request_is" + $(this).attr("value")).attr("value", "1");
        
        $(".content_Type").hide();
        $("#content_" + $(this).attr("value")).show();
        
        setPrice();
    });
    
    $("#request_photo").bind('change',function(e){
        
        
        if (this.files && this.files[0]) {
            $(".loading").show();
            
            var c = document.getElementById('canvas_test');
            var ctx = c.getContext("2d");
            c.width = c.width;
            
            var file = this.files[0];
            var reader = new FileReader();
            reader.onload = function (e) {
                
                // $('.content_photo_preview').attr('src', e.target.result);
                
                var img = new Image();
                img.src = e.target.result;
                
                
                
                
                var formData = new FormData();
                formData.append("data",file);
                $.ajax({
                    url: 'https://api.projectoxford.ai/vision/v1.0/ocr?language=unk&detectOrientation=true',
                    beforeSend: function (request)
                    {
                        request.setRequestHeader("Ocp-Apim-Subscription-Key", "550f774dcb774819884b4fafdd3f6683");
                    },
                    data: formData,
                    processData: false,
                    contentType: false,
                    dataType: "JSON",
                    type: 'POST',
                    error: function(xhr, ajaxOptions, thrownError){
                        // if (xhr.status == 417) {
                        //     alert("올바르지 않은 이메일 주소입니다.");
                        // }
                        // else if(xhr.status == 412){
                        //     alert("이미 존재하는 이메일 주소입니다.");
                        // }
                        var data = JSON.parse(xhr.responseText);
                        alert("사진 분석을 실패하였습니다.\n" + data.message);
                    }
                }).done(function(data){
                    
                    $photoText = "";
                
                    c.width = img.width;
                    c.height = img.height;
                    
                    
                    ctx.drawImage(img, 0,0);
                    // ctx.rotate(-(data.textAngle * Math.PI / 180));
                    ctx.translate(img.width / 2 , img.height / 2);
                    ctx.rotate((data.textAngle * Math.PI / 180));
                    ctx.translate(-img.width / 2 , -img.height / 2);
                    
                    // ctx.translate(img.width / 2, img.height / 2);
                    
                    
                    
                    // ctx.translate(-img.width / 2 , -img.height / 2);
                    // ctx.translate(-img.width / 2 , -img.height / 2);
                    
                    $(data.regions).each(function(i, regions){
                        $(regions.lines).each(function(j, line){
                            $(line.words).each(function(k, word){
                                $photoText = $photoText + " " + word.text;
                                
                                $box = word.boundingBox.split(',');
                                // ctx.lineWidth="6";
                                ctx.fillStyle="red";
                                ctx.globalAlpha=0.5;
                                ctx.fillRect($box[0],$box[1],$box[2],$box[3]);
                            });
                            $photoText = $photoText + '\n';
                            
                        });
                    }); 
                    // ctx.rotate((data.textAngle * Math.PI / 180));
                    ctx.stroke();
                    
                    $photoText = $.trim($photoText);
                    
                    
                    //$(".content_photo_preview_text").text($photoText);
                    $(".content_photo_preview_text").html("분석 결과 약 " + $photoText.length + " 글자(공백 포함)로 파악되었습니다.<br />만약 올바르지 않다고 판단된다면 직접 가격을 설정할 수 있습니다.<br />사진이 작거나 화질이 좋지 않을 경우, 손글씨일 경우, 글이 여러 방향에서 써진 경우 분석이 어렵습니다. 문서를 스캔한 파일의 경우 최상의 결과를 얻을 수 있습니다.");
                    if($photoText == "")$(".content_photo_preview_text").html($(".content_photo_preview_text").html() + "<br />텍스트가 발견되지 않았습니다.");
                    $(".characterCount").text("약 " + $photoText.length + " 글자");
                    setPrice($photoText);
                    $(".loading").hide();
                    
                });
                
                // $("#frmUser_profilePic").submit();
            }

            reader.readAsDataURL(this.files[0]);
        }
        
        
    });
    
    $("#frmRequest").submit(function(e){
        e.preventDefault();
        startLoading();
        
        $("#request_clientId").val($userInfomation.user_email);
        var nowDate = new Date();
        $("#request_deltaFromDue").val(parseInt(($.datepicker.formatDate( "@", $("#deadlineDate").datepicker('getDate')) - nowDate.getTime())/1000) + 86400);
        
        if($('#request_isText').val() == '0'){
            $('#request_text').removeAttr('name');
        }
        if($('#request_isPhoto').val() == '0'){
            $('#request_photo').removeAttr('name');
        }
        
        
        var form = $(this)[0];
        var formData = new FormData(form);
        $.ajax({
            url: '/api/requests',
            data: formData,
            processData: false,
            contentType: false,
            dataType: "JSON",
            type: 'POST',
            error: function(xhr, ajaxOptions, thrownError){
                // if (xhr.status == 417) {
                //     alert("올바르지 않은 이메일 주소입니다.");
                // }
                // else if(xhr.status == 412){
                //     alert("이미 존재하는 이메일 주소입니다.");
                // }
                endLoading();
                alert("의뢰 실패");
            }
        }).done(function(data){
            endLoading();
            // $("body").prepend('<div class="popup"> <div class="popup_background"></div> <div class="payment"> <div class="div_1-2" style="height: 100%; position: relative; overflow-y: visible;"> <div class="div_1-2_1" style="position: relative;"> <div id="payment_select"> <div class="payment_select t" value="0"> 쿠폰 사용 <span class="payment_select_now" id="payment_select_coupon">없음</span> </div> <div class="payment_select" value="1"> 포인트 사용 <span class="payment_select_now" id="payment_select_point">0</span> </div> <div class="payment_select" value="2"> 결제수단 선택 <span class="payment_select_now selected" id="payment_select_payment">-</span> </div> <div class="payment_sum"> 의뢰 금액($<span id="payment_sum_price"></span>) - 쿠폰($<span id="payment_sum_coupon"></span>) - 포인트($<span id="payment_sum_point"></span>)<br /> <span class="payment_select_now" style="font-weight: bold;">결제 금액: $<span id="payment_sum_amount"></span></span> </div> </div> </div> <div class="div_1-2_2" style="position: relative;"> <div id="payment_tab"> <div class="payment_tab selected" id="payment_tab0"> <span class="payment_tab_title"> 쿠폰번호 입력: </span> <div class="payment_tab_content"> <input type="text" placeholder="없음" id="payment_usingCoupon"/> </div> </div> <div class="payment_tab" id="payment_tab1"> <span class="payment_tab_title"> 현재 내 포인트: <span id="payment_myPoint"></span> </span> <br /> <span class="payment_tab_title"> 사용할 포인트: </span> <div class="payment_tab_content"> <input type="number" step="0.01" min="0" max="100" value="0" id="payment_usingPoint"/> </div> </div> <div class="payment_tab" id="payment_tab2"> <span class="payment_tab_title"> 결제 수단을 선택해주세요. </span> <div class="payment_tab_content"> <div class="payment_buttons"> <button type="button" style="border: 1px solid #ddd;" class="payment_button" id="pay_paypal"><img src="/static/img/payment/paypal.png" style="width: 150px;"/></button> <button type="button" style="border: 1px solid #ddd;" class="payment_button" id="pay_alipay"><img src="/static/img/payment/alipay.png" style="width: 150px;"/></button> </div> <!--또는--> <div class="payment_buttons"> <button type="button" style="border: 1px solid #ddd;" class="payment_button invisible" id="pay_iamport">카드번호 입력</button> </div> <div class="payment_tab_content" id="payment_iamport" style="display: none;"> 카드번호: <input type="text" maxlength="4" size="4" id="payment_card1">- <input type="text" maxlength="4" size="4" id="payment_card2">- <input type="password" maxlength="4" size="4" id="payment_card3">- <input type="password" maxlength="4" size="4" id="payment_card4"> <br /> 유효기간: <input type="text" maxlength="2" size="2" id="payment_month">월 <input type="text" maxlength="4" size="4" id="payment_year">년 <br /> 생년월일(6자리): <input type="text" maxlength="6" size="6" id="payment_birth"> <br /> 비밀번호 앞 두 자리: <input type="password" maxlength="2" size="2" id="payment_pwd"> </div> </div> </div> </div> </div> </div> <button class="payment_submit" value=""> 적용하기 </button> <button class="payment_close"> 취소 </button> </div> </div>');
            
            $("body").prepend('<div class="popup"> <div class="popup_background"></div>' +
            '<div class="payment">' +
            '<div class="div_1-2" style="height: 100%; position: relative; overflow-y: visible;"> ' +
            '<div class="div_1-2_1" style="position: relative;"> ' +
            '<div id="payment_select">' +
            '<div class="payment_select" value="0"> ' +
            '쿠폰 사용 ' +
            '<span class="payment_select_now" id="payment_select_coupon">없음</span> ' +
            '</div> ' +
            '<div class="payment_select selected" value="1">' +
            '포인트 사용 ' +
            '<span class="payment_select_now" id="payment_select_point">0</span> ' +
            '</div>' +
            '<div class="payment_select" value="2"> ' +
            '결제수단 선택 ' +
            '<span class="payment_select_now" id="payment_select_payment">-</span>' +
            '</div> ' +
            '<div class="payment_sum"> ' +
            '의뢰 금액($<span id="payment_sum_price"></span>) - 쿠폰($<span id="payment_sum_coupon"></span>) - 포인트($<span id="payment_sum_point"></span>)<br /> ' +
            '<span class="payment_select_now" style="font-weight: bold;">결제 금액: $<span id="payment_sum_amount"></span></span> ' +
            '</div>' +
            '</div>' +
            '</div> ' +
            '<div class="div_1-2_2" style="position: relative;">' + 
            '<div id="payment_tab"> ' +
            '<div class="payment_tab" id="payment_tab0">' +
            '<span class="payment_tab_title"> 쿠폰번호 입력: </span>' +
            '<div class="payment_tab_content"> ' +
            '<input type="text" placeholder="없음" id="payment_usingCoupon"/> </div>' +
            '</div>' +
            '<div class="payment_tab selected" id="payment_tab1">' +
            '<span class="payment_tab_title"> 현재 내 포인트: <span id="payment_myPoint"></span> </span>' +
            '<br /> ' +
            '<span class="payment_tab_title"> 사용할 포인트: </span>' +
            '<div class="payment_tab_content">' +
            '<input type="number" step="0.01" min="0" max="100" value="0" id="payment_usingPoint"/> ' +
            '</div>' +
            '</div> ' +
            '<div class="payment_tab" id="payment_tab2"> ' +
            '<span class="payment_tab_title"> 결제 수단을 선택해주세요. </span> ' +
            '<div class="payment_tab_content">' +
            '<div class="payment_buttons"> ' +
            '<button type="button" style="border: 1px solid #ddd;" class="payment_button" id="pay_paypal">' +
            '<img src="/static/img/payment/paypal.png" style="width: 150px;"/>' +
            '</button>' +
            '<button type="button" style="border: 1px solid #ddd;" class="payment_button" id="pay_alipay">' +
            '<img src="/static/img/payment/alipay.png" style="width: 150px;"/>' +
            '</button> ' +
            '</div> ' +
            '또는' +
            '<div class="payment_buttons"> ' +
            '<button type="button" style="border: 1px solid #ddd;" class="payment_button" id="pay_iamport">' +
            '카드번호 입력' +
            '</button>' +
            ' </div> ' +
            '<div class="payment_tab_content" id="payment_iamport" style="display: none;"> ' +
            '카드번호:' +
            '<input type="text" maxlength="4" size="4" id="payment_card1">' +
            '- <input type="text" maxlength="4" size="4" id="payment_card2">' +
            '- <input type="password" maxlength="4" size="4" id="payment_card3">' +
            '- <input type="password" maxlength="4" size="4" id="payment_card4"> ' +
            '<br /> ' +
            '유효기간: ' +
            '<input type="text" maxlength="2" size="2" id="payment_month">' +
            '월 ' +
            '<input type="text" maxlength="4" size="4" id="payment_year">' +
            '년' +
            '<br /> ' +
            '생년월일(6자리): ' +
            '<input type="text" maxlength="6" size="6" id="payment_birth">' +
            '<br />' +
            '비밀번호 앞 두 자리:' +
            '<input type="password" maxlength="2" size="2" id="payment_pwd">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<button class="payment_submit" value=""> 적용하기 </button>' +
            '<button class="payment_close"> 취소 </button> ' +
            '</div> ' +
            '</div>');



            $("#payment_sum_coupon").text("0");
            $("#payment_sum_point").text("0");
            $("#payment_sum_amount").text($("#nowPrice").val());
            
            $(".payment_submit").attr("value", data.request_id);
            
            $("#payment_sum_price").text($("#nowPrice").val());
            
            $("#payment_myPoint").text($userInfomation.user_point);
            
            if(parseFloat($("#nowPrice").val()) < parseFloat($userInfomation.user_point)){
                $("#payment_usingPoint").attr("max",$("#nowPrice").val());
                
            }else{
                $("#payment_usingPoint").attr("max",$userInfomation.user_point);
            }
            
            $("#payment_usingPoint").change(function(){
                if(parseFloat( $("#payment_usingPoint").val()) > parseFloat($("#payment_usingPoint").attr("max"))){
                    $("#payment_usingPoint").val( parseFloat($("#payment_usingPoint").attr("max")));
                }
            });
            
            
            $(".payment_submit").click(function(){
                if($(".payment_select.selected").attr("value") == "0"){
                    if($("#payment_usingCoupon").val() == ""){
                        $("#payment_sum_coupon").text("0");
                        
                    }
                    else{
                        alert("등록되지 않은 쿠폰입니다.");
                        $("#payment_sum_coupon").text("0");
                    }
                }else if($(".payment_select.selected").attr("value") == "1"){
                    $("#payment_select_point").text($("#payment_usingPoint").val());
                    $("#payment_sum_point").text($("#payment_usingPoint").val());
                    
                }else if($(".payment_select.selected").attr("value") == "2"){
                    //$(".payment_button.selected")
                    
                    formData = new FormData();
                    
                    
                    if($("#payment_select_point").text() == "-") {
                        alert("사용할 포인트를 먼저 적용해주세요.");
                        return;
                    }
                    
                    if($("#payment_select_payment").text() == "-") {
                        alert("결제 수단을 선택해주세요.");
                        return;
                    }
                    else if($("#payment_select_payment").text() == "PayPal"){
                        formData.append("pay_via", "paypal");             
                    }
                    else if($("#payment_select_payment").text() == "Alipay"){
                        formData.append("pay_via", "alipay");             
                    }
                    else if($("#payment_select_payment").text() == "직접입력"){
                        formData.append("pay_via", "iamport");
                        formData.append("card_number", $("#payment_card1").val() + "-" + $("#payment_card2").val() + "-" + $("#payment_card3").val() + "-" + $("#payment_card4").val());
                        formData.append("expiry", $("#payment_year").val() + "-" + $("#payment_month").val());
                        formData.append("birth", $("#payment_birth").val());
                        formData.append("pwd_2digit", $("#payment_pwd").val());
                    }
                    else if($("#payment_select_payment").text() == "포인트로만"){
                        formData.append("pay_via", "point_only");             
                    }
                    
                    formData.append("pay_amount", parseFloat($("#payment_sum_price").text()));             
                    formData.append("use_point", parseFloat($("#payment_select_point").text()));
                    formData.append("pay_by", "web");
                    
            
                    startLoading();
                    $.ajax({
                        url: '/api/user/requests/'+$(this).attr("value")+'/payment/start',
                        data: formData,
                        processData: false,
                        contentType: false,
                        dataType: "JSON",
                        type: 'POST',
                        error: function(xhr, ajaxOptions, thrownError){
                            // if (xhr.status == 417) {
                            //     alert("올바르지 않은 이메일 주소입니다.");
                            // }
                            // else if(xhr.status == 412){
                            //     alert("이미 존재하는 이메일 주소입니다.");
                            // }
                            endLoading();
                            alert("의뢰 실패");
                        }
                    }).done(function(data){
                        endLoading();
                        location.href = data.link;
                    });
                }
                
                $("#payment_sum_amount").text(parseFloat($("#payment_sum_price").text())-parseFloat($("#payment_sum_coupon").text())-parseFloat($("#payment_sum_point").text()));
                if(parseFloat($("#payment_sum_amount").text()) <= 0){
                    $("#payment_select_payment").text("포인트로만");
                }

                $(".payment_select")[2].click();
            });
            
            $(".payment_button").click(function(){
                
                if($("#payment_select_payment").text() == "포인트로만"){
                    $(".payment_button").removeClass("selected");
                    $("#payment_iamport").hide();
                    return;
                }
                
                $(".payment_button").removeClass("selected");
                $(this).addClass("selected");
                
            });
            
            $("#pay_paypal").click(function(){
                if($("#payment_select_payment").text() == "포인트로만")return;
                $("#payment_select_payment").text("PayPal");
                $("#payment_iamport").hide();
            });
            $("#pay_alipay").click(function(){
                if($("#payment_select_payment").text() == "포인트로만")return;
                $("#payment_select_payment").text("Alipay");
                $("#payment_iamport").hide();
            });
            $("#pay_iamport").click(function(){
                if($("#payment_select_payment").text() == "포인트로만")return;
                $("#payment_select_payment").text("직접입력");
                $("#payment_iamport").show();
            });
            
           
            
            $(".payment_close").click(function(){
               $(".popup").remove(); 
            });
            
            
            $(".payment_select").click(function(){
                $(".payment_select").removeClass("selected");
                $(this).addClass("selected");
                $(".payment_tab").removeClass("selected");
                $("#payment_tab" + $(this).attr("value")).addClass("selected");
                if($(this).attr("value") == '2'){
                    $('.payment_submit').text("결제하기");
                }
                else{
                    $('.payment_submit').text("적용하기");
                }
            });
            
            // $(".payment").click(function(){
                
            // })
            
            $(".payment_submit").click();
        });
    });
    
    if(localStorage.getItem("content").length>0){
        $("#request_text").val(localStorage.getItem("content"));
        $("#selectLanguageFrom").ddslick('select', {index: localStorage.getItem("originalLang") });
        $("#selectLanguageTo").ddslick('select', {index: localStorage.getItem("targetLang") });
        $("#request_text").change();
        localStorage.clear();
    }
    
    
    $('body').off("selectstart");
    $('body').off("dragstart");
    $('body').off("contextmenu"); 
    
    setPrice();
});

function setPrice($text) {
    if(!$text && $("#request_isPhoto") == 1){
        $text = $photoText;
    }
    $text = $text || $("#request_text").val();
    // if($text.length <= 140){

    //     $("#priceBar").attr("min",10);



    //     // if($("#request_isText").val()!="1"){
            
    //     //     // $(".priceMin").text("USD 10.00부터~");
    //     //     $("#priceBar").attr("min",10);
            
    //     // }
    //     // else{
            
    //     //     // $(".priceMin").text("USD 0.00부터~");
    //     //     $("#priceBar").attr("min",0);
    //     // }
    // }
    // else if($text.length <= 166){
    //     $("#priceBar").attr("min",10);
    //     // $(".priceMin").text("USD 10.00부터~");
    // }
    // else
    {
        // alert('lang_' + $('input[name=request_originalLang]').val() + '_countBy');
        if(i18n.getI18n('lang_' + $('input[name=request_originalLang]').val() + '_countBy') == 'letter'){
            $("#priceBar").attr("min",(parseFloat(($text.length * parseFloat(i18n.getI18n('lang_' + $('input[name=request_originalLang]').val() + '_price'))))).toFixed(2));
        }
        else if(i18n.getI18n('lang_' + $('input[name=request_originalLang]').val() + '_countBy') == 'word'){
            $("#priceBar").attr("min",(parseFloat(($text.replace(/^[\s,.;]+/, "").replace(/[\s,.;]+$/, "").split(/[\s,.;]+/).length * parseFloat(i18n.getI18n('lang_' + $('input[name=request_originalLang]').val() + '_price'))))).toFixed(2));
            
        }

    }
    
    $(".priceMin").text("USD "+(parseFloat( $("#priceBar").attr("min") )).toFixed(2)+"부터~");

    $("#priceBar").attr("max",parseFloat( $("#priceBar").attr("min") )* 2 + 99.99);
    if (parseFloat($("#priceBar").attr("min")) >= parseFloat($("#nowPrice").val()))
        $("#nowPrice").val(parseFloat($("#priceBar").attr("min")).toFixed(2));
    
}


