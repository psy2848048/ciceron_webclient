
$(document).ready(function(){
    i18n.setLanguage($.cookie($userInfomation.user_email + "translate"));
    var isEdit = false;
    
    
    $.ajax({
        url: $apiURL + '/complete/groups',
        processData: false,
        contentType: false,
        cache : false,
        dataType: "JSON",
        type: 'GET',
        error: function(xhr, ajaxOptions, thrownError){
            // if (xhr.status == 417) {
            //     alert("올바르지 않은 이메일 주소입니다.");
            // }
            // else if(xhr.status == 412){
            //     alert("이미 존재하는 이메일 주소입니다.");
            // }
            // alert("에러!");
        }
    }).done(function(data){
        var lastId = 0;
        // $((data.data).reverse()).each(function(i, item) {
        $((data.data)).each(function(i, item) {
            $("#folder").prepend('<div class="folder" value="'+item.id+'"><span class="folder_title">'+item.name+'</span><span class="folder_delete" value="'+item.id+'"><i class="icon ion-ios-minus-outline fa-2x"></i></span></div>');
            lastId = item.id;
        });
        loadGroupList(lastId);
        
        $('.folder').on('contextmenu', function(){
            $('.folder_edit').click();
        });
        
        $('.folder').click(function(e){
            // alert($(this).attr('value'));
            
            if(isEdit){
                if(e.target.className == "folder_delete"||e.target.className == "icon ion-ios-minus-outline fa-2x"){
                    return;
                }
                    
                if($(this).find('.folder_title')[0].nodeName == "INPUT") {
                    return;
                }
                
                var attrs = { };
                
                $.each($(this).find('.folder_title')[0].attributes, function(idx, attr) {
                    attrs[attr.nodeName] = attr.nodeValue;
                });
                
                attrs['type'] = 'text';
                attrs['value'] = $(this).find('.folder_title').text();
                
                $(this).find('.folder_title').replaceWith(function () {
                    return $("<input />", attrs).append($(this).contents());
                });
                
                
                $(this).find('.folder_title').select();
                
                $(this).find('input.folder_title').on('blur', function(){
                    $(this).off();
                    var what = $(this);
                    var formData = new FormData();
                    formData.append('group_name', $(this).val())
                    
                    $.ajax({
                        url: $apiURL + '/complete/groups/' + $(this).parent().attr("value"),
                        processData: false,
                        data: formData,
                        contentType: false,
                        cache : false,
                        dataType: "JSON",
                        type: 'PUT',
                        error: function(xhr, ajaxOptions, thrownError){
                            // if (xhr.status == 417) {
                            //     alert("올바르지 않은 이메일 주소입니다.");
                            // }
                            // else if(xhr.status == 412){
                            //     alert("이미 존재하는 이메일 주소입니다.");
                            // }
                            alert("에러가 발생하였습니다.");
                            location.reload();
                        }
                    }).done(function(data){
                        what.text(what.val());
                        $.each(what[0].attributes, function(idx, attr) {
                            attrs[attr.nodeName] = attr.nodeValue;
                        });
                        what.replaceWith(function () {
                            return $("<span />", attrs).append(what.contents());
                        });
                        //location.reload();
                    });
                    
                    
                    
                    
                    
                        
                });
                
                $(this).find('input.folder_title').on('keyup', function(e){
                    if(e.which == 13){
                        $(this).blur();
                    }
                });
                
            }
            else{
                loadGroupList($(this).attr("value"));
            }
        });
        
        
        $(".folder_delete").click(function(){
            
            $("body").prepend('<div class="popup invisible" id="popup'+$(this).attr("value")+'"><div class="popup_background"></div><div class="dialog"><div class="dialog_image"><img src="/static/img/9.7.1feedback3_web.png" /></div><span class="dialog_message">정말로 삭제하시겠습니까?<br />해당 폴더 내의 번역물들은 기본 폴더로 이동됩니다.</span><div class="dialog_buttons"><button type="button" class="positive">아니오</button><button type="button" class="negative" value="'+$(this).attr("value")+'">예</button></div></div></div>');
            
            $("#popup"+$(this).attr("value")).delay(1).queue(function(){$(this).removeClass("invisible");});
                        
            $("#popup"+$(this).attr("value")).find(".negative").click(function(){
                $.ajax({
                    url: $apiURL + '/complete/groups/' + $(this).attr("value"),
                    processData: false,
                    contentType: false,
                    cache : false,
                    dataType: "JSON",
                    type: 'DELETE',
                    error: function(xhr, ajaxOptions, thrownError){
                        // if (xhr.status == 417) {
                        //     alert("올바르지 않은 이메일 주소입니다.");
                        // }
                        // else if(xhr.status == 412){
                        //     alert("이미 존재하는 이메일 주소입니다.");
                        // }
                        alert("에러!");
                    }
                }).done(function(data){
                    location.reload();
                });
            });
            $("#popup"+$(this).attr("value")).find(".positive").click(function(){
                $(".popup").remove();
            });
        });
    });
    
    
    
    $('.folder_add').click(function(){
       $("body").prepend('<div class="popup invisible" id="popup"><div class="popup_background"></div><div class="dialog"><form class="frmAddFolder" method="POST"> <!--<div class="dialog_image"><img src="/static/img/9.7.1feedback3_web.png" /></div>--><span class="dialog_message">새 폴더 이름을 지정해주세요.</span><input type="text" name="group_name" class="dialog_inputText" /><div class="dialog_buttons"><button type="submit" class="positive">만들기</button><button type="button" class="negative">취소</button></div></form></div></div>'); 
       
        $("#popup").delay(1).queue(function(){$("#popup").removeClass("invisible");});
        $(".frmAddFolder").submit(function(e){
            e.preventDefault();
            
            var form = $(this)[0];
            var formData = new FormData(form);
            
            $.ajax({
                url: $apiURL + '/complete/groups',
                data: formData,
                processData: false,
                contentType: false,
                dataType: "JSON",
                type: 'POST',
                success: function(data){
                    location.reload();
                },
                error: function(xhr, ajaxOptions, thrownError){
                    if (xhr.status == 401) {
                        alert(xhr.message);
                    }
                    else{
                        alert("알 수 없는 에러로 폴더 만들기 실패하였습니다.");
                    }
                }
            });
        });
        $(".negative").click(function(){
            $("#popup").remove();
        });
    });
    
    $('.folder_edit').click(function(){
        if($('.folder').hasClass('edit')){
            $('.folder').removeClass('edit');
            isEdit = false;
            $(this).find('.folder_title').html('<i class="ion-compose"></i>');
        }
        else{
            $('.folder').addClass('edit');
            isEdit = true;
            $(this).find('.folder_title').html('변경 완료');
        }
    });
    
    
    
    
    $('.completed_contentTextWrap').click(function(){
        if($(this).css('height') != '80px'){
            $(this).css('height','80px');
        }
        else{
            $('.completed_contentTextWrap').css('height','80px');
            $(this).css('height','auto');
        }
        //
        
    });
    
    function loadGroupList($groupId){
        //<div id="completed"> <div class="completed"> <div class="completed_header"> <div class="completed_121_1"> <span style="padding-top: 10px; margin: 0px; margin-top: 10px;">2015.02.09 15:03</span> </div> <div class="completed_121_2 invisible1024"> </div> <div class="completed_121_1 right"> <div class="completed_type"></div> </div> </div> <hr class="invisible1024"/> <div class="completed_content"> <div class="completed_121_1"> <div class="completed_infomation"> <div style="width: 100%; background-color:#eee;"> <div class="completed_language"> <img src="/static/img/flags/round/cn.png" style="height: 30px; border-radius: 50%;" /> <p style="font-size: 8px; padding: 0px;">한국어</p> </div> <div style="float: left; width: 10px; padding-top: 20px;"> <img src="/static/img/flags/round/cn.png" style="width: 10px; border-radius: 50%;" /> </div> <div class="completed_language"> <img src="/static/img/flags/round/cn.png" style="height: 30px; border-radius: 50%;" /> <p style="font-size: 8px; padding: 0px;">중국어</p> </div> </div> <div style="width: 100%; background-color: transparent;"> <div class="completed_contentType"> <img src="/static/img/flags/round/cn.png" style="height: 30px; border-radius: 50%;" /> <p style="font-size: 8px; padding: 0px;">출판물</p> </div> <div style="float: left; width: 10px; padding-top: 20px;"> </div> <div class="completed_contentType"> <img src="/static/img/flags/round/cn.png" style="height: 30px; border-radius: 50%;" /> <p style="font-size: 8px; padding: 0px;">과학</p> </div> </div> <p class="completed_wordCount">420 단어</p> </div> </div> <div class="completed_121_2"> <span class="completed_contentTitle"> 최근 번역물 </span> <div class="completed_contentTextWrap"> <span class="completed_contentText"> 안녕하세여;; 1+1=귀요미 랍니다... 믿기 어려우시겠지만 그렇다능 호호호호 I can not speak Korean. Please help me. I can not speak Korean. Please help me. I can not speak Korean. Please help me. </span> </div> </div> <div class="completed_121_1 right"> <div class="completed_button">확인</div> </div> </div> </div> </div>
        
        
        $.ajax({
            url: $apiURL + '/complete/groups/' + $groupId,
            processData: false,
            contentType: false,
            cache : false,
            dataType: "JSON",
            type: 'GET',
            error: function(xhr, ajaxOptions, thrownError){
                // if (xhr.status == 417) {
                //     alert("올바르지 않은 이메일 주소입니다.");
                // }
                // else if(xhr.status == 412){
                //     alert("이미 존재하는 이메일 주소입니다.");
                // }
                alert("에러!");
            }
        }).done(function(data){
            $("#completed").empty();
            
            if(!Object.keys(data.data).length){
                $("#completed").prepend("결과가 없습니다.");
                return;
            }
            $(data.data).each(function(i, item) {
                $("#completed").prepend('<div class="completed buttons" id="completed'+item.request_id+'"> <div class="completed_header"> <div class="completed_121_1"> <span style="padding-top: 10px; margin: 0px; margin-top: 10px;">'+(new Date(item.request_submittedTime)).toLocaleString()+'</span> </div> <div class="completed_121_2 invisible1024"> </div> <div class="completed_121_1 right"> <div class="completed_type"></div> </div> </div> <hr class="invisible1024"/> <div class="completed_content"> <div class="completed_121_1"> <div class="completed_infomation"> <div style="width: 100%; background-color:#eee;"> <div class="completed_language"> <img src="/static/img/flags/round/'+item.request_originalLang+'.png" style="height: 30px; border-radius: 50%;" /> <p style="font-size: 8px; padding: 0px;">'+i18n.getI18n("lang_" + item.request_originalLang)+'</p> </div> <div style="float: left; width: 10px; padding-top: 20px;"> <img src="/static/img/flags/round/arrow.png" style="width: 10px; border-radius: 50%;" /> </div> <div class="completed_language"> <img src="/static/img/flags/round/'+item.request_targetLang+'.png" style="height: 30px; border-radius: 50%;" /> <p style="font-size: 8px; padding: 0px;">'+i18n.getI18n("lang_" + item.request_targetLang)+'</p> </div> </div> '+(item.request_isSos?'':'<div style="width: 100%; background-color: transparent;"> <div class="completed_contentType"> <img src="/static/img/format/'+item.request_format+'.png" style="height: 30px; border-radius: 50%;" /> <p style="font-size: 8px; padding: 0px;">'+ i18n.getI18n('format_' + item.request_format)+'</p> </div> <div style="float: left; width: 10px; padding-top: 20px;"> </div> <div class="completed_contentType"> <img src="/static/img/subject/'+item.request_subject+'.png" style="height: 30px; border-radius: 50%;" /> <p style="font-size: 8px; padding: 0px;">'+ i18n.getI18n('subject_' + item.request_subject)+'</p> </div> </div>')+' <p class="completed_wordCount">'+item.request_words+' 단어</p> </div> </div> <div class="completed_121_2"> <span class="completed_contentTitle">' + (item.request_title?item.request_title:"최근 번역물") + '</span> <div class="completed_contentTextWrap"> <span class="completed_contentText">'+ item.request_context +'</span> </div> </div> <div class="completed_121_1 right"> <!--<div class="completed_button">확인</div>--> </div> </div> </div>');
                
                $("#completed" + item.request_id).click(function(){
                   showTicket(item.request_id) ;
                });
            });
        });
    }
    
});

