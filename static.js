var connect = require('connect'),
    serveStatic = require('serve-static'),
    connectRoute = require('connect-route'),
    morgan = require('morgan'),
    path = require('path'),
    fs = require("fs"),
    ext = require("./ext"),
	__dirname = './static';

var logger = morgan('dev')

// /static 으로 시작하는 주소일 경우 
var app = connect();
app.use(logger);
app.use(requestHandler);
app.use(serveStatic(__dirname));
app.use(connectRoute(function (router) {
    router.get('/', function (req, res, next) {
        res.end('index');
    });
		
		router.get('/terms', function (req, res, next) {
			fs.readFile(__dirname + "/terms.html", "utf8", function(err, file) {
				res.writeHead(200, {
					"Content-Type": "text/html;charset=UTF-8"
				});
				res.write(file, "utf8");
				res.end();
			});
    });
		router.get('/privacy', function (req, res, next) {
			fs.readFile(__dirname + "/privacy.html", "utf8", function(err, file) {
				res.writeHead(200, {
					"Content-Type": "text/html;charset=UTF-8"
				});
				res.write(file, "utf8");
				res.end();
			});
    });
		
    router.get('/stoa', function (req, res, next) {
		fs.readFile(__dirname + "/stoa.1.html", "utf8", function(err, file) {
			res.writeHead(200, {
				"Content-Type": "text/html;charset=UTF-8"
			});
			res.write(file, "utf8");
			res.end();
		});
    });
		
    router.get('/translate', function (req, res, next) {
		fs.readFile(__dirname + "/translate.html", "utf8", function(err, file) {
			res.writeHead(200, {
				"Content-Type": "text/html;charset=UTF-8"
			});
			res.write(file, "utf8");
			res.end();
		});
    });
    
    router.get('/teaser', function (req, res, next) {
		fs.readFile(__dirname + "/index.html", "utf8", function(err, file) {
			res.writeHead(200, {
				"Content-Type": "text/html;charset=UTF-8"
			});
			res.write(file, "utf8");
			res.end();
		});
    });

    router.get('/stoa/:id', function (req, res, next) {
		fs.readFile(__dirname + "/stoa.html", "utf8", function(err, file) {
			res.writeHead(200, {
				"Content-Type": "text/html;charset=UTF-8"
			});
			res.write(file, "utf8");
			res.end();
		});
    }); 
 
    router.get('/about', function (req, res, next) {
		fs.readFile(__dirname + "/about.2.html", "utf8", function(err, file) {
			res.writeHead(200, {
				"Content-Type": "text/html;charset=UTF-8"
			});
			res.write(file, "utf8");
			res.end();
		});
    });
    
    router.get('/notification', function (req, res, next) {
		fs.readFile(__dirname + "/notification.html", "utf8", function(err, file) {
			res.writeHead(200, {
				"Content-Type": "text/html;charset=UTF-8"
			});
			res.write(file, "utf8");
			res.end();
		});
    });
    
    router.get('/profile', function (req, res, next) {
		fs.readFile(__dirname + "/profile.html", "utf8", function(err, file) {
			res.writeHead(200, {
				"Content-Type": "text/html;charset=UTF-8"
			});
			res.write(file, "utf8");
			res.end();
		});
    });
    
    router.get('/makerequest', function (req, res, next) {
		fs.readFile(__dirname + "/makerequest.html", "utf8", function(err, file) {
			res.writeHead(200, {
				"Content-Type": "text/html;charset=UTF-8"
			});
			res.write(file, "utf8");
			res.end();
		});
    });
    
    
    router.get('/ticket/:id', function (req, res, next) {
		fs.readFile(__dirname + "/ticket.html", "utf8", function(err, file) {
			res.writeHead(200, {
				"Content-Type": "text/html;charset=UTF-8"
			});
			res.write(file, "utf8");
			res.end();
		});
    });
    
    router.get('/request', function (req, res, next) {
		fs.readFile(__dirname + "/request.html", "utf8", function(err, file) {
			res.writeHead(200, {
				"Content-Type": "text/html;charset=UTF-8"
			});
			res.write(file, "utf8");
			res.end();
		});
    });
    
    router.get('/mypage', function (req, res, next) {
		fs.readFile(__dirname + "/mypage.html", "utf8", function(err, file) {
			res.writeHead(200, {
				"Content-Type": "text/html;charset=UTF-8"
			});
			res.write(file, "utf8");
			res.end();
		});
    });
    
    //
    
    router.get('/status', function (req, res, next) {
		fs.readFile(__dirname + "/status.html", "utf8", function(err, file) {
			res.writeHead(200, {
				"Content-Type": "text/html;charset=UTF-8"
			});
			res.write(file, "utf8");
			res.end();
		});
    });
    
    router.get('/completed', function (req, res, next) {
		fs.readFile(__dirname + "/completed.html", "utf8", function(err, file) {
			res.writeHead(200, {
				"Content-Type": "text/html;charset=UTF-8"
			});
			res.write(file, "utf8");
			res.end();
		});
    });
    
    router.get('/processingrequests', function (req, res, next) {
		fs.readFile(__dirname + "/processingrequests.html", "utf8", function(err, file) {
			res.writeHead(200, {
				"Content-Type": "text/html;charset=UTF-8"
			});
			res.write(file, "utf8");
			res.end();
		});
    });
  
    router.get('/processingrequests/:id', function (req, res, next) {
		fs.readFile(__dirname + "/processingrequests.html", "utf8", function(err, file) {
			res.writeHead(200, {
				"Content-Type": "text/html;charset=UTF-8"
			});
			res.write(file, "utf8");
			res.end();
		});
    });
      
    router.get('/cart', function (req, res, next) {
		fs.readFile(__dirname + "/cart.html", "utf8", function(err, file) {
			res.writeHead(200, {
				"Content-Type": "text/html;charset=UTF-8"
			});
			res.write(file, "utf8");
			res.end();
		});
    });

    router.get('/translating', function (req, res, next) {
		fs.readFile(__dirname + "/translating.html", "utf8", function(err, file) {
			res.writeHead(200, {
				"Content-Type": "text/html;charset=UTF-8"
			});
			res.write(file, "utf8");
			res.end();
		});
    });

    router.get('/translating/:id', function (req, res, next) {
		fs.readFile(__dirname + "/translating.html", "utf8", function(err, file) {
			res.writeHead(200, {
				"Content-Type": "text/html;charset=UTF-8"
			});
			res.write(file, "utf8");
			res.end();
		});
    });
    
    router.get('/donerequests', function (req, res, next) {
		fs.readFile(__dirname + "/donerequests.html", "utf8", function(err, file) {
			res.writeHead(200, {
				"Content-Type": "text/html;charset=UTF-8"
			});
			res.write(file, "utf8");
			res.end();
		});
    });        

    router.get('/donerequests/:id', function (req, res, next) {
		fs.readFile(__dirname + "/donerequests.html", "utf8", function(err, file) {
			res.writeHead(200, {
				"Content-Type": "text/html;charset=UTF-8"
			});
			res.write(file, "utf8");
			res.end();
		});
    });
    
    router.get('/activity', function (req, res, next) {
		fs.readFile(__dirname + "/activity.html", "utf8", function(err, file) {
			res.writeHead(200, {
				"Content-Type": "text/html;charset=UTF-8"
			});
			res.write(file, "utf8");
			res.end();
		});
    });    

    router.get('/activity/:id', function (req, res, next) {
		fs.readFile(__dirname + "/activity.html", "utf8", function(err, file) {
			res.writeHead(200, {
				"Content-Type": "text/html;charset=UTF-8"
			});
			res.write(file, "utf8");
			res.end();
		});
    });
    
    router.get('/landing', function (req, res, next) {
		fs.readFile(__dirname + "/landing.html", "utf8", function(err, file) {
			res.writeHead(200, {
				"Content-Type": "text/html;charset=UTF-8"
			});
			res.write(file, "utf8");
			res.end();
		});
    });      
}));

app.use(function(req, res){
	fs.readFile(__dirname + "/404.html", "utf8", function(err, file) {
      var headers = {};
      var contentType = ext.getContentType([path.extname(__dirname + "/404.html")][0].replace(".",""));
      if (contentType) headers["Content-Type"] = contentType+';charset=UTF-8';
      res.writeHead(200, headers);
      res.write(file, "utf8");
      res.end();
    });
});
app.listen(15000);

function requestHandler(req, res, next) {
	if (isStaticUrl(req.url)) {
		req.url = req.url.replace('/static', '');
	}
	next();
}

function isStaticUrl(url) {
	return (url === '/favicon.ico' || url.substring(0, 7) === '/static');
}



