var stampToTime = function (leftTime) {
  var MINUTE = 60,
      HOUR = 60 * 60,
      DAY = 60 * 60 * 24;

  var time = 0, timeUnit = '';

  leftTime = leftTime >> 0;
  if (leftTime < HOUR) {
    time = leftTime / MINUTE;
    timeUnit = ' 분';
  } else if (leftTime < DAY) {
    time = leftTime / HOUR;
    timeUnit = ' 시간';
  } else {
    time = leftTime / DAY;
    timeUnit = ' 일';
  }

  time = Math.round(time);
  // time = time.toFixed(1); // 소수점 반올림

  return time + timeUnit;
};

var offerPopupStr = function (context, originLang, originLangCode, transLang, transLangCode, price, leftTime, originText) {
  for (var i = 0; i < arguments.length; i++) {
    arguments[i] = 'ㅋㅋ';
  }

  leftTime = stampToTime(leftTime);

  return '<div class="popup" id="popup-1">' +
         '<div class="popup-dim"></div>' +
         '<div class="detail">' +
         '<div class="detail-close text-right">' +
         '<button class="close-popup rm-button-style">' +
         '<i class="icon ion-ios-close-empty fa-4x"></i>' +
         '</button>' +
         '</div>' +
         '<div class="detail-header">' +
         '<span class="pull-left">Information</span>' +
         // '<span class="pull-right">' + 'date' + '</span>' +
         '</div>' +
         '<hr />' +
         '<div class="detail-title">Context</div>' +
         '<div class="detail-content">' +
         context +
         '</div>' +
         '<div class="detail-informations clearfix">' +
         '<div class="detail-information">' +
         '<div class="detail-information-title text-center clearfix">언어</div>' +
         '<hr />' +
         '<div class="detail-information-icons clearfix">' +
         '<div class="detail-information-icon">' +
         '<img class="img-circle" src="/img/flags/round/' + originLangCode + '.png" />' +
         '<p>' + originLang + '</p>' +
         '</div>' +
         '<div class="detail-information-arrow">' +
         '<img class="img-circle" src="/img/flags/round/arrow.png" />' +
         '</div>' +
         '<div class="detail-information-icon">' +
         '<img class="img-circle" src="/img/flags/round/' + transLangCode + '.png" />' +
         '<p>' + transLang + '</p>' +
         '</div>' +
         '</div>' +
         '</div>' +
         '<div class="detail-information">' +
         '<div class="detail-information-title text-center clearfix">가격</div>' +
         '<hr />' +
         '<div class="detail-information-words text-center clearfix">' + price + '</div>' +
         '</div>' +
         '<div class="detail-information">' +
         '<div class="detail-information-title text-center clearfix">남은시간</div>' +
         '<hr />' +
         '<div class="detail-information-words text-center clearfix">' + leftTime + '</div>' +
         '</div>' +
         '</div>' +
         '<div class="detail-title">Original Text</div>' +
         '<div class="detail-content">' + originText + '</div>' +
         '<div class="clearfix"><div class="pull-right"><img class="popup-logo" src="/img/1.1.1logo_web_n.png" /></div></div>' +
         '<div class="text-center">' +
         '<button class="ok-button one-of-two rm-button-style">번역승락</button>' +
         '<button class="no-button one-of-two rm-button-style">번역취소</button>' +
         '</div>'
         '</div>' +
         '</div>';
};

var l = function () {
  var i = 0;
  for (; i < arguments.length; i++) {
    console.log('===================================================================');
    console.log((i + 1) + '.');
    console.log(arguments[i]);
    console.log('===================================================================');
  }
};

var utcToLocal = function (utc) {
  var date = new Date(utc);
  return date;
};

var setOfferPrice = function (length) {
  var textLength = length || $('.detail-content').eq(1).text().trim().length;

  if (textLength <= 140) {
    $('.minimum-price').text('USD 0.00부터~');
    $('#offer-price-bar').attr('min', 0);
  } else if (textLength <= 166) {
    $('.minimum-price').text('USD 10.00부터~');
    $('#offer-price-bar').attr('min', 10);
  } else {
    $('.minumum-price').text('USD ' + (parseFloat((textLength * 0.06))).toFixed(2) + "부터~");
    $('#offer-price-bar').attr('min', (parseFloat((textLength * 0.06))).toFixed(2));
  }

  $('#offer-price-bar').attr('max', parseFloat($('#offer-price-bar').attr('min')) * 2 + 99.99);
  $('#offered-price').val((parseFloat($('#offer-price-bar').attr('min'))).toFixed(2));
};

var showTicket = function ($id) {
  $.ajax({
    url: '/api/user/requests/' + $id,
    processData: false,
    contentType: false,
    cache : false,
    async: true,
    dataType: 'JSON',
    type: 'GET',
    error: function() {
      // location.href = "/about";
    }
  }).done(function (data) {
    // alert(data.data[0].request_id);
    $(data.data).each(function (key, value) {
      var requestSOSText = '<div class="detail-information">' +
                           '<div class="detail-information-title text-center clearfix">포맷</div><hr />' +
                           '<div class="detail-information-icons">' +
                           '<div class="detail-information-icon">' +
                           '<img class="img-circle" src="/img/format/' + value.request_format + '.png" />' +
                           '<p>' + i18n.getI18n('format_' + value.request_format) + '</p>' +
                           '</div>' +
                           '<div class="detail-information-arrow"></div>' +
                           '<div class="detail-information-icon">' +
                           '<img src="/img/subject/' + value.request_subject + '.png" />' +
                           '<p>' + i18n.getI18n('subject_' + value.request_subject) + '</p>' +
                           '</div>' +
                           '</div>' +
                           '</div>';
      var requestNormalText = '<div class="detail-information">' +
                              '<div class="detail-information-title text-center clearfix">포맷</div><hr />' +
                              '<div class="detail-information-title text-center clearfix">무료단문번역</div>' +
                              '</div>';

      var requestIsSOSText = value.request_isSos ? requestSOSText : requestNormalText;

      $('body').prepend('<div class="popup invisible" id="popup-' + value.request_id + '">' +
                        '<div class="popup-dim"></div>' +
                        '<div class="detail">' +
                        '<div class="detail-close text-right">' +
                        '<button class="close-popup rm-button-style">' +
                        '<i class="icon ion-ios-close-empty fa-4x"></i>' +
                        '</button>' +
                        '</div>' +
                        '<div class="detail-header"' +
                        '<span class="pull-left"> Information </span>' +
                        '<span class="pull-right"> 2015.02.01 15:03 </span>' +
                        '</div>' +
                        '<hr />' +
                        '<div class="detail-title">Context</div>' +
                        '<div class="detail-content">' + value.request_context + '</div>' +
                        '<div class="detail-informations clearfix">' +
                        '<div class="detail-information">' +
                        '<div class="detail-information-title text-center clearfix">언어</div><hr />' +
                        '<div class="detail-information-icons clearfix">' +
                        '<div class="detail-information-icon">' +
                        '<img class="img-circle" src="/img/flags/round/' + value.request_originalLang + '.png" />' +
                        '<p>' + i18n.getI18n("lang_" + value.request_originalLang) + '</p>' +
                        '</div>' +
                        '<div class="detail-information-arrow"><img class="img-circle" src="/img/flags/round/arrow.png" /></div>' +
                        '<div class="detail-information-icon">' +
                        '<img class="img-circle" src="/img/flags/round/' + value.request_targetLang + '.png" />' +
                        '<p>' + i18n.getI18n("lang-" + value.request_targetLang) + '</p>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        requestIsSOSText +
                        '<div class="detail-information">' +
                        '<div class="detail-information-title text-center clearfix">단어수</div> <hr />' +
                        '<div class="detail-information-words text-center clearfix">' + value.request_words + '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="detail-title">Original Text</div>' +
                        '<div class="detail-content">' + value.request_text + '</div>' +
                        '</div>' +
                        '</div>');

      $('#popup-' + value.request_id).delay(1).queue(function () {
        $('#popup-' + value.request_id).removeClass('invisible');
      });

      $('#popup-' + value.request_id).find('.detail-close i').click(function () {
        $('#popup-' + value.request_id).remove();
      });
    });
  });
};

var getRequestsList = function (t) {
  $.ajax({
    url: '/api/user/requests/stoa?page=1',
    processData: false,
    contentType: false,
    cache: false,
    dataType: "JSON",
    type: 'GET',
    error: function (xhr, ajaxOptions, thrownError) {
      // if (xhr.status == 417) {
      //     alert("올바르지 않은 이메일 주소입니다.");
      // }
      // else if(xhr.status == 412){
      //     alert("이미 존재하는 이메일 주소입니다.");
      // }
      // alert("에러!");
    }
  }).done(function (data) {
    $(data.data).each(function (key, value) {
      $('#tickets').append('<div class="ticket">' +
                           '<div class="ticket-header">' +
                           '<div class="col-lg-4 col-sm-10">' +
                           '<span class="user-name">' + value.request_clientName +'</span>' +
                           '</div>' +
                           '<div class="col-lg-4 none-sm">' +
                           '<span class="user-request">context</span>' +
                           '</div>' +
                           '<div class="col-lg-2 col-sm-10 text-right top-right">' +
                           '<div class="free-translate">무료단문번역</div>' +
                           // '<div class="charge-translate">$ 가격</div>' +
                           '</div>' +
                           '</div>' +
                           '<hr class="none-sm">' +
                           '<div class="ticket-content clearfix">' +
                           '<div class="col-lg-4 col-sm-10">' +
                           '<div class="ticket-profile pull-left">' +
                           '<img class="user-image img-circle" src="/api/access_file/' + value.request_clientPicPath + '">' +
                           '<div class="left-time">' +
                           '<p>남은 시간</p>' +
                           '<p>' + (new Date(value.request_dueTime - (new Date()).getTime())).getMinutes() + '</p>' +
                           '</div>' +
                           '</div>' +
                           '<div class="ticket-information pull-left">' +
                           '<div class="ticket-language-information clearfix">' +
                           '<div class="ticket-language pull-left">' +
                           '<img class="img-circle" src="/img/flags/round/' + value.request_originalLang + '.png" alt="country flag">' +
                           '<p class="locale">' + i18n.getI18n('lang_' + value.request_originalLang) + '</p>' +
                           '</div>' +
                           '<div class="ticket-language-direction pull-left">' +
                           '<img class="arrow img-circle" src="/img/flags/round/arrow.png" alt="arrow">' +
                           '</div>' +
                           '<div class="ticket-language pull-left">' +
                           '<img class="img-circle" src="/img/flags/round/' + value.request_targetLang + '.png" alt="country flag">' +
                           '<p class="locale">' + i18n.getI18n('lang_' + value.request_targetLang) + '</p>' +
                           '</div>' +
                           '</div>' +
                           /***** content type 부분*****/
                           '<!--' +
                           '<div class="ticket-content-type">' +
                           '<img class="img-circle" src="/img/flags/round/arrow.png" />' +
                           '<p>출판물</p>' +
                           '</div>' +
                           '<div class="ticket-content-type-blank"></div>' +
                           '<div class="ticket-content-type">' +
                           '<img class="img-circle" src="/img/flags/round/arrow.png" />' +
                           '<p">과학</p>' +
                           '</div>' +
                           '-->' +
                           /***** content type 부분*****/
                           '<p class="ticket-word-count">' + value.request_words + ' 단어' + '</p>' +
                           '</div>' +
                           '</div>' +
                           '<div class="col-lg-4 col-sm-10">' +
                           '<div class="ticket-content-text-wrapper">' +
                           '<span class="ticket-content-text">' + value.request_words + '</span>' +
                           '</div>' +
                           '</div>' +
                           /***** 가격 제시한 사람들 부분*****/
                           '<!--' +
                           '<div class="applier-wrapper col-lg-2 col-sm-10 clearfix">' +
                           '<span class="applier pull-left text-right col-lg-5 col-sm-2">' +
                           '<img class="applier-image img-circle" src="/img/flags/round/1.png"/><br/>' +
                           '<span class="applied-cost">+$10</span>' +
                           '<div class="applier-information">' +
                           '<span class="applier-name">이름 : ' + '김개똥' + '</span><br />' +
                           '<span class="applier-score">채택률 : ' + '10%' + '</span>' +
                           '</div>' +
                           '</span>' +
                           '</div>' +
                           '-->' +
                           /***** 가격 제시한 사람들 부분*****/
                           '<div class="col-lg-2 col-sm-10 text-right bottom-right">' + (new Date(value.request_registeredTime)).toLocaleString() + '</div>' +
                           '</div>' +
                           '</div>');
    })});
};

var commonReady = function () {
  var $userInformation;

  var scrollTimer = null;

  var functionScroll = function (e) {
    var $nowScrollTop = $(this).scrollTop();
    if ($nowScrollTop > 10) {
      $('#header').addClass('scrolled');
    } else {
      $('#header').removeClass('scrolled');
    }
  };

  $(window).scroll(function (e) {
    if (scrollTimer) {
      clearTimeout(scrollTimer);   // clear any previous pending timer
    }

    scrollTimer = setTimeout(functionScroll, 5);   // set new timer
  });

  $('body').on('touchmove', function () {
    if (scrollTimer) {
      clearTimeout(scrollTimer);   // clear any previous pending timer
    }

    scrollTimer = setTimeout(functionScroll, 5);   // set new timer
  });

  functionScroll();

  $(window).on('popstate', function (e) {
    if (!$('#header-profile').hasClass('invisible')) {
      $('#header-profile').addClass('invisible');
    }
  });

  $('a[href|="' + $(location).attr('pathname') + '"]').addClass('selected');

  $('body').on('selectstart', function (e) { return false; });

  $('body').on('contextmenu', function(e) { return false; });

  $('body').on('click', '.popup-dim, .popup .detail-close button, .popup .close-popup, .popup .dialog .close', function () {
    $('.popup').remove();
  });

  $('body').on('click', '.show-detail-information', function () {
    var $slidingEl = $(this).closest('.detail-title').next();

    if ($($slidingEl).hasClass('collapsed')) {
      $($slidingEl).removeClass('collapsed');
      $(this).find('i').addClass('ion-ios-minus').removeClass('ion-ios-plus');
    } else {
      $($slidingEl).addClass('collapsed');
      $(this).find('i').removeClass('ion-ios-minus').addClass('ion-ios-plus');
    }
  });

  $('body').on('click', '.translate-button', function () {
    $('body').append(offerPopupStr(0,0,0,0,0,0,0,0,0,0,0,0));
  });

  $(document).bind('mousedown', function (e) {
    if (e.target.id != 'img-header' && !$('#header-profile').find(e.target).length) {
      if (!$('#header-profile').hasClass('invisible')) {
        $('#header-profile').addClass('invisible');
        history.back();
      }
    }
  });

  $('body').on('click', '#img-header', function () {
    if ($('#header-profile').hasClass('invisible')) {
      var obj = { Page: "CICERON", Url: location.href };

      $('#header-profile-container').css('max-height', $(window).outerHeight() - 90 + 'px');
      $('#header-profile').removeClass('invisible');

      history.pushState(obj, obj.Page, obj.Url);
    } else {
      $('#header-profile').addClass('invisible');
      history.back();
    }
  });

  $('body').on('click', '#btn-logout', function () {
    $.ajax({
      url: '/api/logout',
      processData: false,
      contentType: false,
      cache : false,
      dataType: 'JSON',
      type: 'GET',
      error: function () {
        alert('잠시 후 시도해주세요.');
      }
    }).done(function (data) {
      location.href = '/about';
    });
  });

  $('body').on('click', '.apply-translating', function () {
    var targetId = $(this).attr('data-id');
    $('body').prepend('<div class="popup" id="popup-' + targetId + '">' +
                      '<div class="popup" id="popup-1">' +
                      '<div class="popup-dim"></div>' +
                      '<div class="dialog">' +
                      '<div class="dialog-close text-right">' +
                      '<button class="close-popup rm-button-style">' +
                      '<i class="icon ion-ios-close-empty fa-4x"></i>' +
                      '</button>' +
                      '</div>' +
                      '<div class="offering-wrapper">' +
                      '<div class="dialog-title">' +
                      '가격 제시하기' +
                      '</div>' +
                      '<div class="dialog-content clearfix">' +
                      '<form>' +
                      '<div class="price-input-wrapper">' +
                      '<input class="price-input" id="offer-price-bar" type="range" min="0" max="99.99" step="0.01" value="0" name="rating" />' +
                      '<div class="minimum-price"></div>' +
                      '</div>' +
                      '<div class="price-input-wrapper">' +
                      '<label for="offered-price">$ </label>' +
                      '<input class="price-input rm-button-style" id="offered-price" type="text" />' +
                      '</div>' +
                      '<div class="price-input-wrapper col-lg-10 col-sm-10">' +
                      '<input class="rm-button-style" type="submit" value="제시하기" />' +
                      '</div>' +
                      '</form>' +
                      '</div>' +
                      '</div>' +
                      '</div>' +
                      '</div>' +
                      '</div>');

    setOfferPrice($(this).closest('.ticket-content').find('.ticket-content-text').text().trim().length);
  });

  $('body').on('input change', '#offer-price-bar', function () {
    $('#offered-price').val((parseFloat($(this).val())).toFixed(2));
  });

  $('body').on('input change', '#offered-price', function () {
    $('#offer-price-bar').val((parseFloat($(this).val())).toFixed(2));
  });

  setOfferPrice();

  $.ajax({
    url: '/api/user/profile',
    processData: false,
    contentType: false,
    cache : false,
    async: false,
    dataType: 'JSON',
    type: 'GET',
    error: function () {
      // TEMP
      // location.href = "/about";
    }
  }).done(function (data) {
    $userInformation = data;

    $('#img-header').attr('src', '/api/access_file/' + $userInformation.user_profilePicPath);
    $('#img-profile').attr('src', '/api/access_file/' + $userInformation.user_profilePicPath);
    $('#header-profile-name').text($userInformation.user_name);
  });

  $.ajax({
    url: '/api/notification',
    processData: false,
    contentType: false,
    cache : false,
    dataType: 'JSON',
    type: 'GET',
    error: function (){
      //location.href = "/about";
    }
  }).done(function (data) {
    if (data.numberOfNoti > 0 && $(location).attr('pathname') !== '/status') {
      $('#btnStatus').append('<i class="fa fa-circle" style="color: #FE8B97; font-size: 5px; position: absolute;"></i>');
      $('#btnStatus').addClass("shake-slow shake-constant shake-constant--hover");
    }

    $('body').on('click', 'a', function (e) {
      // e = e || window.event;
      // e.preventDefault();
      // var obj = { Page: 'CICERON', Url: $(this).attr('href') };
      // history.pushState(obj, obj.Page, obj.Url);
      // location.replace(obj.Url);
      // $.ajax({ url: obj.Url }).done(function (data) {
      //   $('html').html(data);
      // });

      // $('html').empty();
      // $('html').append('<iframe style="width: 100%; height: 1000px; border: none;"></iframe>')
      // $('iframe').attr('src', obj.Url);
    });
  });

  $('head').append('<link rel="stylesheet" href="/css/csshake.min.css">');

  i18n.setLanguage('ko');
};

$(document).ready(commonReady);

