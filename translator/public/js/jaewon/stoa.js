var ticketContentTypeStr = function (contentTypeCode, contentType, subjectCode, subject) {
  return '<div class="ticket-content-type-information clearfix">' +
         '<div class="ticket-content-type pull-left">' +
         '<img class="img-circle" src="/img/flags/round/' + contentTypeCode + '.png" alt="country flag">' +
         '<p class="locale">' + contentType + '</p>' +
         '</div>' +
         '<div class="ticket-content-type-blank pull-left"></div>' +
         '<div class="ticket-content-type pull-left">' +
         '<img class="img-circle" src="/img/flags/round/' + subjectCode + '.png" alt="country flag">' +
         '<p class="locale">' + subject + '</p>' +
         '</div>' +
         '</div>';
};

var ticketTranslatorStr = function (arr) {
  var template = '';

  var i = 0;
  for (; i < arr.length; i++) {
    template += '<span class="applier pull-left text-right col-lg-5 col-sm-2">' +
                '<img class="applier-image img-circle" src="/img/flags/round/1.png"/><br/>' +
                '<span class="applied-cost none">+$10</span>' +
                '<div class="applier-information">' +
                '<span class="applier-name">' +
                '이름 : ' + arr[i].name +
                '</span>' +
                '<br />' +
                '<span class="applier-score">' +
                '채택률 : ' + arr[i].hitRatio + ' %' +
                '</span>' +
                '</div>' +
                '</span>';
  }

  return template;
};

var ticketStr = function (obj) {
  var template = '';

  var i = 0;
  for (; i < obj.length; i++) {
    var contentTypeStr = '', translatorStr = '';

    if (obj[i].contentType && obj[i].contentTypeCode && obj[i].subject && obj[i].subjectCode) {
      contentTypeStr = ticketContentTypeStr(obj[i].contentTypeCode, obj[i].contentType, obj[i].subjectCode, obj[i].subject);
    }

    if (obj[i].accepter && obj[i].accepter.length) {
      translatorStr = ticketTranslatorStr(obj[i].accepter);
    }

    template += '<div class="ticket">' +
                '<div class="ticket-header">' +
                '<div class="col-lg-3 col-sm-10">' +
                '<span class="user-name">' + obj[i].userName + '</span>' +
                '</div>' +
                '<div class="col-lg-4 none-sm">' +
                '<span class="user-request">context</span>' +
                '</div>' +
                '<div class="col-lg-3 col-sm-10 text-right top-right">' +
                '<div class="cart-button">장바구니</div>' +
                '<div class="translate-button">바로번역</div>' +
                '</div>' +
                '</div>' +
                '<hr class="none-sm">' +
                '<div class="ticket-content clearfix">' +
                '<div class="col-lg-3 col-sm-10">' +
                '<div class="ticket-profile pull-left">' +
                '<img class="user-image img-circle" src="' + obj[i].userImagePath + '">' +
                '<div class="left-time">' +
                '<p>남은 시간</p>' +
                '<p>' + stampToTime(obj[i].leftTime) + '</p>' +
                '</div>' +
                '</div>' +
                '<div class="ticket-information pull-left">' +
                '<div class="ticket-language-information clearfix">' +
                '<div class="ticket-language pull-left">' +
                '<img class="img-circle" src="/img/flags/round/' + obj[i].originLangCode + '.png" alt="country flag">' +
                '<p class="locale">' + obj[i].originLang + '</p>' +
                '</div>' +
                '<div class="ticket-language-direction pull-left">' +
                '<img class="arrow img-circle" src="/img/flags/round/arrow.png" alt="arrow">' +
                '</div>' +
                '<div class="ticket-language pull-left">' +
                '<img class="img-circle" src="/img/flags/round/' + obj[i].transLangCode + '.png" alt="country flag">' +
                '<p class="locale">' + obj[i].transLang + '</p>' +
                '</div>' +
                '</div>' +
                contentTypeStr +
                '<p class="ticket-word-count">' + obj[i].wordCount + ' 단어</p>' +
                '</div>' +
                '</div>' +
                '<div class="col-lg-4 col-sm-10">' +
                '<div class="ticket-content-text-wrapper">' +
                '<span class="ticket-content-text">' + obj[i].context + '</span>' +
                '</div>' +
                '</div>' +
                /***** 가격 제시한 사람들 *****/
                '<div class="applier-wrapper col-lg-3 col-sm-10 clearfix">' +
                '<div class="cart-wrapper col-lg-5 col-sm-5 clearfix">' +
                '</div>' +
                '<div class="col-lg-5 col-sm-5 clearfix">' +
                translatorStr +
                /*
                '<span class="applier pull-left text-right col-lg-5 col-sm-2">' +
                '<img class="applier-image img-circle" src="/img/flags/round/1.png"/><br/>' +
                '<span class="applied-cost none">+$10</span>' +
                '<div class="applier-information">' +
                '<span class="applier-name">' +
                '이름 : ' + obj[i].accepter.name +
                '</span>' +
                '<br />' +
                '<span class="applier-score">' +
                '채택률 : ' + obj[i].accepter.hitRatio + ' %' +
                '</span>' +
                '</div>' +
                '</span>' +
                */
                /***** 가격 제시한 사람들 *****/
                '</div>' +
                '</div>' +
                /***** 가격 제시하기 *****/
                /*
                '<div class="apply-button-wrapper col-lg-3 col-sm-10 clearfix">' +
                '<div class="pull-left text-right col-lg-10 col-sm-10">' +
                '<button class="apply-translating rm-button-style" data-id="' + obj[i].id + '">제시하기</button>' +
                '</div>' +
                '</div>' +
                */
                /***** 가격 제시하기 *****/
                '<div class="col-lg-3 col-sm-10 text-right bottom-right">' + obj[i].date + '</div>' +
                '</div>' +
                '</div>';
  }

  return template;
};

var ready = function () {
  $.ajax({
    url: '/ticket 받아오기',
    data: '',
    processData: false,
    contentType: false,
    dataType: "JSON",
    type: 'POST',
    success: function(data){
    },
    error: function (xhr, ajaxOptions, thrownError) {
      $('#tickets').append(ticketStr(fakeData.tickets));
    }
  });

  /*
  $('.ticket-content-text-wrapper').click(function () {
    if ($(this).css('height') !== '135px') {
      $(this).css('height','135px');
    } else {
      $('.ticket_contentTextWrap').css('height','135px');
      $(this).css('height','auto');
    }
  });
  */

  // ddslick 플러그인: select box 이쁘게~~
  $('#select-language-from').ddslick({
    width: 120,
    onSelected: function(selectedData){
      // callback function: do something with selectedData;
    }
  });

  $('#select-language-to').ddslick({
    width: 120,
    onSelected: function(selectedData){
      //callback function: do something with selectedData;
    }
  });

  // request textarea 키입력될 때마다 글자 수 내용 변경해줌
  $('body').on('keyup keydown change', '#content', function () {
    $('.character-count').text($(this).val().length + " / 140");
    if ($(this).val().length > 140) $('.character-count').css('color', '#f00');
    else $('.character-count').css('color', '#000');
  });

  $('body').on('submit', '#request-form', function (e) {
    e.preventDefault();

    // 140자 이상 입력했을 시 request로 전달하기 위해 localStorage에 저장
    if ($('#content').val().length > 140) {
      localStorage.setItem('content', $('#content').val());
      localStorage.setItem('originalLang', $('#select-language-from').data('ddslick').selectedIndex);
      localStorage.setItem('targetLang', $('#select-language-to').data('ddslick').selectedIndex);
      location.replace("/request");
      return;
    }

    $('#request-client-id').val($userInfomation.user_email);

    var form = $(this)[0];
    var formData = new FormData(form);

    $.ajax({
        url: '/api/requests',
        data: formData,
        processData: false,
        contentType: false,
        dataType: "JSON",
        type: 'POST',
        success: function(data){
          alert('작성 성공');
          location.href = '/stoa';
        },
        error: function (xhr, ajaxOptions, thrownError) {
          // if (xhr.status == 417) {
          //   alert("올바르지 않은 이메일 주소입니다.");
          // }
          // else if(xhr.status == 412){
          //   alert("이미 존재하는 이메일 주소입니다.");
          // }
          alert('작성 에러!');
        }
    });
  });

  $('body').on('click', '.applier', function () {
    $(this).toggleClass('active');
  });
  // getRequestsList(1);
};

$(document).ready(ready);

