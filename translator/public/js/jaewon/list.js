var folderList = [];
var transTextObj = {};

var getFolders = function () {
  $.ajax({
    url: '/폴더 받아오기',
    data: '',
    processData: false,
    contentType: false,
    dataType: 'JSON',
    type: 'POST',
    success: function (data) {
    },
    error: function (data) {
      $('.folder-wrapper').append(setFolders(fakeData.folders));
      getCard(0);
    }
  });
};

var getCard = function (folderId) {
  $.ajax({
    url: '/카드 받아오기',
    data: '',
    processData: false,
    contentType: false,
    dataType: 'JSON',
    type: 'POST',
    success: function (data) {
    },
    error: function (data) {
      $('#translate-list .background-tile').append(getCards(fakeData.cards));
    }
  });
};

var setFolders = function (obj) {
  var template = '<hr />';

  var i = 0;
  for (; i < obj.length; i++) {
    template += '<button class="folder rm-button-style">' + obj[i].name + '</button>';
  }

  return template;
};

var cardContentTypeStr = function (contentTypeCode, contentType, subjectCode, subject) {
  return '<div class="text-content-type-information clearfix">' +
         '<div class="text-content-type pull-left">' +
         '<img class="img-circle" src="/img/flags/round/' + contentTypeCode + '.png" alt="country flag">' +
         '<p class="locale">' + contentType + '</p>' +
         '</div>' +
         '<div class="text-content-type-blank pull-left"></div>' +
         '<div class="text-content-type pull-left">' +
         '<img class="img-circle" src="/img/flags/round/' + subjectCode + '.png" alt="country flag">' +
         '<p class="locale">' + subject + '</p>' +
         '</div>' +
         '</div>';
};

var getCards = function (obj) {
  var template = '';

  var i = 0;
  for (; i < obj.length; i++) {
    var o = obj[i], contentTypeStr = '';

    if (o.contentType && o.contentTypeCode && o.subject && o.subjectCode) {
      contentTypeStr = cardContentTypeStr(o.contentTypeCode, o.contentType, o.subjectCode, o.subject);
    }

    template += '<div class="translate-card-wrapper">' +
                '<div class="translate-card" data-id="' + o.id + '">' +
                '<div class="card-header">' +
                o.name +
                '</div>' +
                '<div class="card-container">' +
                '<div class="card-content clearfix">' +
                '<div class="user-info col-lg-5 col-sm-5">' +
                '<div><img class="user-image img-circle pull-left" src="' + o.userImage + '" /></div>' +
                '<div class="result-wrapper">' +
                '<img class="result-image img-circle pull-left" src="' + o.userImage + '" />' +
                '<div class="talk-balloon none"><div class="talk-balloon-text">' + o.userAnswer + '</div><div class="talk-balloon-triangle"><div></div></div></div>' +
                '</div>' +
                '</div>' +
                '<div class="text-info col-lg-5 col-sm-5">' +
                '<div class="text-information pull-left">' +
                '<div class="text-language-information clearfix">' +
                '<div class="text-language pull-left">' +
                '<img class="img-circle" src="/img/flags/round/' + o.originLangCode + '.png" alt="country flag">' +
                '<p class="locale">' + o.originLang + '</p>' +
                '</div>' +
                '<div class="text-language-direction pull-left">' +
                '<img class="arrow img-circle" src="/img/flags/round/arrow.png" alt="arrow">' +
                '</div>' +
                '<div class="text-language pull-left">' +
                '<img class="img-circle" src="/img/flags/round/' + o.transLangCode + '.png" alt="country flag">' +
                '<p class="locale">' + o.transLang + '</p>' +
                '</div>' +
                '</div>' +
                contentTypeStr +
                '<p class="text-word-count">' + o.wordCount + ' 단어</p>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="translate-date">' + o.date + '</div>' +
                '<div class="more-info-background"></div>' +
                '<div class="more-info">…</div>' +
                '</div>' +
                '<div class="card-function-menu clearfix collapsed">' +
                '<div class="move-card text-center">' +
                '<select class="move-to rm-button-style">' +
                '<option value="1">기본폴더</option>' +
                '<option value="2">폴더2</option>' +
                '</select>' +
                '</div>' +
                '<div class="show-card text-center">' +
                '더보기' +
                '</div>' +
                '<div class="delete-card text-center">' +
                '<button class="fa fa-trash rm-button-style"></button>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';
  }

  return template;
};

var showCard = function (id) {
  $.ajax({
    url: '/카드 팝업 열기',
    data: '',
    processData: false,
    contentType: false,
    dataType: 'JSON',
    type: 'POST',
    success: function (data) {
    },
    error: function (data) {
      $('body').append(getCardPopup(fakeData.translatedCard));
      setMemoPosition();
    }
  });
};

var showSplitView = function (obj) {
  var template = '';

  var i = 0;
  for (; i < obj.length; i++) {
    var o = obj[i], originStr = '', transStr = '', opinionStr = '', memoStr = '', mixStr = '';

    if (o.paragraph_comment) {
      opinionStr = '<p class="content-opinion" data-seq-id="' + o.paragraph_seq + '">' +
                   '<span>' + o.paragraph_comment + '</span>' +
                   '</p>';
    }

    var j = 0;
    for (; j < o.sentences.length; j++) {
      var oo = o.sentences[j], hasMemoStr = '';
      if (oo.sentence_comment) {
        memoStr += '<div class="sentence-memo none" data-seq-id="' + o.paragraph_seq + '-' + oo.sentence_seq + '">' +
                   '<div class="memo-wrapper">' +
                   '<div class="memo-content">' + oo.sentence_comment + '</div>' +
                   '<div class="memo-triangle"></div>' +
                   '</div>' +
                   '</div>';

        memoStrInToggled = '<div class="sentence-memo none" data-seq-id="' + o.paragraph_seq + '-' + oo.sentence_seq + '">' +
                           '<div class="memo-wrapper">' +
                           '<div class="memo-content">' + oo.sentence_comment + '</div>' +
                           '<div class="memo-triangle"></div>' +
                           '</div>' +
                           '</div>';

        hasMemoStr = '<span class="has-memo"></span>';
      }

      originStr += '<span class="content-sentence" data-seq-id="' + o.paragraph_seq + '-' + oo.sentence_seq + '">' +
                   hasMemoStr +
                   '<span>' + oo.original_text + '</span>' +
                   '<div class="memo-in-toggled">' +
                   (memoStrInToggled.length ? memoStrInToggled : '') +
                   '</div>' +
                   '</span>';

      transStr += '<span class="content-sentence" data-seq-id="' + o.paragraph_seq + '-' + oo.sentence_seq + '">' +
                  '<span>' + oo.translated_text + '</span>' +
                  '</span>';

      mixStr += '<span class="content-sentence origin" data-seq-id="' + o.paragraph_seq + '-' + oo.sentence_seq + '">' +
                hasMemoStr +
                '<span>' + oo.original_text + '</span>' +
                '</span>' +
                '<div class="memo-in-toggled">' +
                (memoStrInToggled.length ? memoStrInToggled : '') +
                '</div>' +
                '<span class="content-sentence trans" data-seq-id="' + o.paragraph_seq + '-' + oo.sentence_seq + '">' +
                '<span>' + oo.translated_text + '</span>' +
                '</span>';
    }

    template += '<div class="content-paragraph clearfix">' +
                '<div class="left-side">' +
                memoStr +
                '</div>' +
                '<div class="right-side">' +
                '<div class="origin-paragraph col-lg-5 col-sm-5">' +
                originStr +
                '</div>' +
                '<div class="trans-paragraph col-lg-5 col-sm-5">' +
                transStr +
                opinionStr +
                '</div>' +
                '<div class="mix-paragraph col-lg-5 col-sm-5">' +
                mixStr +
                opinionStr +
                '</div>' +
                '</div>' +
                '</div>';
  }

  return template;
};

var getCardPopup = function (obj) {
  var template = '';

  template = '<div class="popup" id="popup-1">' +
             '<div class="popup-dim"></div>' +
             '<div class="detail">' +
             '<div class="detail-close text-right">' +
             '<button class="close-popup rm-button-style">' +
             '<i class="icon ion-ios-close-empty fa-4x"></i>' +
             '</button>' +
             '</div>' +
             '<div class="detail-header clearfix">' +
             '<div class="left-side"></div>' +
             '<div class="right-side">' +
             '<span class="title">번역문</span>' +
             '<hr />' +
             '<div class="left-guidance col-lg-5 col-sm-10 clearfix">' +
             '<button class="toggle-memo fa fa-youtube rm-button-style"></button>' +
             '<button class="show-split select-view-type fa fa-facebook rm-button-style active" data-view-type="split-view"></button>' +
             '<button class="show-vertical select-view-type fa fa-twitter rm-button-style" data-view-type="vertical-view"></button>' +
             '<button class="show-sentence select-view-type fa fa-pinterest rm-button-style" data-view-type="sentence-view"></button>' +
             '</div>' +
             '<div class="right-guidance col-lg-5 col-sm-10 text-right clearfix">' +
             '<div class="color-guide show-origin-text">원문보기</div>' +
             '<div class="color-guide show-trans-text">본문보기</div>' +
             '<div class="color-guide show-opinion">문단견해</div>' +
             '</div>' +
             '</div>' +
             '</div>' +
             '<div class="detail-content split-view clearfix">' +
             showSplitView(obj);
             '</div>' +
             '</div>' +
             '</div>';

  return template;
};

var ready = function () {
  getFolders();

  $('body').on('mouseenter touchstart', '.result-image', function () {
    var $cardContainer = $(this).closest('.card-container');

    $($cardContainer).find('.user-image, .text-info, .translate-date').addClass('get-dimed');
    $($cardContainer).find('.talk-balloon').removeClass('none');
  });

  $('body').on('mouseout touchend', '.result-image', function () {
    var $cardContainer = $(this).closest('.card-container');

    $($cardContainer).find('.user-image, .text-info, .translate-date').removeClass('get-dimed');
    $($cardContainer).find('.talk-balloon').addClass('none');
  });

  $('body').on('click', '.more-info', function () {
    var $card = $(this).closest('.translate-card');

    $($card).find('.more-info, .more-info-background').addClass('transparent');
    $($card).find('.card-function-menu').removeClass('collapsed');
  });

  $('body').on('click', '.show-card', function () {
    var $card = $(this).closest('.translate-card');
    var cardId = $($card).attr('data-id');

    showCard(cardId);
  });

  $('body').on('click', '.toggle-memo', function () {
    $(this).closest('.popup').find('.left-side').toggleClass('collapsed');
    $(this).closest('.popup').find('.right-side').toggleClass('opened');
  });

  $('body').on('click', '.select-view-type', function () {
    $('.select-view-type').removeClass('active');
    $(this).addClass('active');
    $('.detail-content').removeClass('split-view vertical-view sentence-view').addClass($(this).attr('data-view-type'));

    $('.detail-content .selected').removeClass('selected');
    $('.detail-content .has-memo').removeClass('none').removeClass('active');
    $('.detail-content .sentence-memo').addClass('none');

    setMemoPosition();
  });

  $('body').on('click', '.show-origin-text', function () {
    if ($('.show-trans-text').hasClass('selected') && $('.show-opinion').hasClass('selected')) {
      return;
    }

    if ($(this).hasClass('selected')) {
      $('.origin-paragraph *, .content-sentence.origin, .content-sentence.origin *').removeClass('transparent');
      $(this).removeClass('selected');
    } else {
      $('.origin-paragraph *, .content-sentence.origin, .content-sentence.origin *').addClass('transparent');
      $(this).addClass('selected');
    }
  });

  $('body').on('click', '.show-trans-text', function () {
    if ($('.show-origin-text').hasClass('selected') && $('.show-opinion').hasClass('selected')) {
      return;
    }

    if ($(this).hasClass('selected')) {
      $('.trans-paragraph .content-sentence, .trans-paragraph .content-sentence *, .content-sentence.trans, .content-sentence.trans *').removeClass('transparent');
      $(this).removeClass('selected');
    } else {
      $('.trans-paragraph .content-sentence, .trans-paragraph .content-sentence *, .content-sentence.trans, .content-sentence.trans *').addClass('transparent');
      $(this).addClass('selected');
    }
  });

  $('body').on('click', '.show-opinion', function () {
    if ($('.show-origin-text').hasClass('selected') && $('.show-trans-text').hasClass('selected')) {
      return;
    }

    if ($(this).hasClass('selected')) {
      $('.trans-paragraph .content-opinion, .trans-paragraph .content-opinion *, .mix-paragraph .content-opinion, .mix-paragraph .content-opinion *').removeClass('transparent');
      $(this).removeClass('selected');
    } else {
      $('.trans-paragraph .content-opinion, .trans-paragraph .content-opinion *, .mix-paragraph .content-opinion, .mix-paragraph .content-opinion *').addClass('transparent');
      $(this).addClass('selected');
    }
  });

  $('body').on('click', '.popup .origin-paragraph .content-sentence span:not(.has-memo)', function () {
    var sentenceId = $(this).parent().attr('data-seq-id');
    var $another = $(this).closest('.right-side').find('.trans-paragraph .content-sentence[data-seq-id="' + sentenceId + '"] span:not(.has-memo)');
    var $memo = $(this).closest('.content-paragraph').find('.sentence-memo[data-seq-id="' + sentenceId + '"]');
    var $memoButton = $(this).parent().find('.has-memo');

    $(this).toggleClass('selected');
    $($another).toggleClass('selected');

    $($memoButton).toggleClass('active');

    toggleSentenceMemo($memo, this);
    setMemoPosition();
  });

  $('body').on('click', '.popup .trans-paragraph .content-sentence span:not(.has-memo)', function () {
    var sentenceId = $(this).parent().attr('data-seq-id');
    var $another = $(this).closest('.right-side').find('.origin-paragraph .content-sentence[data-seq-id="' + sentenceId + '"] span:not(.has-memo)');
    var $memo = $(this).closest('.content-paragraph').find('.sentence-memo[data-seq-id="' + sentenceId + '"]');
    var $memoButton = $($another).parent().find('.has-memo');

    $(this).toggleClass('selected');
    $($another).toggleClass('selected');

    $($memoButton).toggleClass('active');

    toggleSentenceMemo($memo, $another);
    setMemoPosition();
  });

  $('body').on('click', '.popup .origin-paragraph .content-sentence .has-memo', function () {
    var sentenceId = $(this).parent().attr('data-seq-id');
    var $anotherOne = $(this).closest('.right-side').find('.origin-paragraph').find('.content-sentence[data-seq-id="' + sentenceId + '"] span:not(.has-memo)');
    var $anotherTwo = $(this).closest('.right-side').find('.trans-paragraph').find('.content-sentence[data-seq-id="' + sentenceId + '"] span:not(.has-memo)');
    var $memo = $(this).closest('.content-paragraph').find('.sentence-memo[data-seq-id="' + sentenceId + '"]');

    $($anotherOne).toggleClass('selected');
    $($anotherTwo).toggleClass('selected');

    $(this).toggleClass('active');

    toggleSentenceMemo($memo, $anotherOne);
    setMemoPosition();
  });

  $('body').on('click', '.popup .mix-paragraph .content-sentence.origin span:not(.has-memo)', function () {
    var sentenceId = $(this).parent().attr('data-seq-id');
    var $another = $(this).closest('.right-side').find('.mix-paragraph .content-sentence.trans[data-seq-id="' + sentenceId + '"] span:not(.has-memo)');
    var $memo = $(this).closest('.content-paragraph').find('.sentence-memo[data-seq-id="' + sentenceId + '"]');
    var $memoButton = $(this).parent().find('.has-memo');

    $(this).toggleClass('selected');
    $($another).toggleClass('selected');

    $($memoButton).toggleClass('active');

    toggleSentenceMemo($memo, this);
    setMemoPosition();
  });

  $('body').on('click', '.popup .mix-paragraph .content-sentence.trans span:not(.has-memo)', function () {
    var sentenceId = $(this).parent().attr('data-seq-id');
    var $another = $(this).closest('.right-side').find('.mix-paragraph .content-sentence.origin[data-seq-id="' + sentenceId + '"] span:not(.has-memo)');
    var $memo = $(this).closest('.content-paragraph').find('.sentence-memo[data-seq-id="' + sentenceId + '"]');
    var $memoButton = $($another).parent().find('.has-memo');

    $(this).toggleClass('selected');
    $($another).toggleClass('selected');

    $($memoButton).toggleClass('active');

    toggleSentenceMemo($memo, $another);
    setMemoPosition();
  });

  $('body').on('click', '.popup .mix-paragraph .content-sentence .has-memo', function () {
    var sentenceId = $(this).parent().attr('data-seq-id');
    var $anotherOne = $(this).closest('.right-side').find('.mix-paragraph').find('.content-sentence.origin[data-seq-id="' + sentenceId + '"] span:not(.has-memo)');
    var $anotherTwo = $(this).closest('.right-side').find('.mix-paragraph').find('.content-sentence.trans[data-seq-id="' + sentenceId + '"] span:not(.has-memo)');
    var $memo = $(this).closest('.content-paragraph').find('.sentence-memo[data-seq-id="' + sentenceId + '"]');

    $($anotherOne).toggleClass('selected');
    $($anotherTwo).toggleClass('selected');

    $(this).toggleClass('active');

    toggleSentenceMemo($memo, $anotherOne);
    setMemoPosition();
  });

  var toggleSentenceMemo = function (memo, origin) {
    var y = -1 * ($(origin).closest('.right-side').offset().top - $(origin).offset().top);

    $(memo).toggleClass('none').css('top', y + 'px');
  };
};

var setMemoPosition = function () {
  var els = $('.has-memo');

  var i = 0;
  for (; i < els.length; i++) {
    var el = $(els.eq(i));
    var y = -1 * ($(el).closest('.right-side').offset().top - $(el).parent().find('span:not(.has-memo)').offset().top);

    $(el).css('top', (y + 5) + 'px');
  }
};

$(document).ready(ready);

