requirejs.config({
    baseUrl: '/static/js/lib',
    paths: {
        app: '../app'
    }
});

requirejs(['jquery', 'app/domain', 'app/AuthService'] , function($, domain, authService) {
	
	authService.setUserProfile(null);

	// 라우팅... 블록 or 리다이렉팅 
	authService.getUserProfile({
		success: function(data) { 
			location.href='/stoa';
		},
		error: function(){
			location.href='/about';
		}
	});
});
