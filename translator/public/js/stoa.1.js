$(document).ready(function(){
  i18n.setLanguage('ko');

  $('.ticket_contentTextWrap').click(function(){
    if($(this).css('height') != '135px'){
      $(this).css('height','135px');
    }
    else{
      $('.ticket_contentTextWrap').css('height','135px');
      $(this).css('height','auto');
    }
  });

  //ddslick 플러그인: select box 이쁘게~~
  $('#select-language-from').ddslick({
      width: 120,
      onSelected: function(selectedData){
          //callback function: do something with selectedData;
      }
  });

  $('#select-language-to').ddslick({
      width: 120,
      onSelected: function(selectedData){
          //callback function: do something with selectedData;
      }
  });

  //request textarea 키입력될 때마다 글자 수 내용 변경해줌
  $("#content").on('keyup keydown change',function(){
      $(".characterCount").text($(this).val().length + " / 140");
      if($(this).val().length>140) $(".characterCount").css("color","red");
      else $(".characterCount").css("color","black");
  });

  $("#request-form").submit(function(e){
      e.preventDefault();

      //140자 이상 입력했을 시 request로 전달하기 위해 localStorage에 저장
      if($("#content").val().length>140){
          localStorage.setItem("content",$("#content").val());
          localStorage.setItem("originalLang",$("#select-language-from").data('ddslick').selectedIndex);
          localStorage.setItem("targetLang",$("#select-language-to").data('ddslick').selectedIndex);
          location.replace("/request");
          return;
      }

      $("#request_clientId").val($userInfomation.user_email);

      var form = $(this)[0];
      var formData = new FormData(form);

      $.ajax({
          url: '/api/requests',
          data: formData,
          processData: false,
          contentType: false,
          dataType: "JSON",
          type: 'POST',
          success: function(data){
              alert("작성 성공");
              location.href = "/stoa";
          },
          error: function(xhr, ajaxOptions, thrownError){
              // if (xhr.status == 417) {
              //     alert("올바르지 않은 이메일 주소입니다.");
              // }
              // else if(xhr.status == 412){
              //     alert("이미 존재하는 이메일 주소입니다.");
              // }
              alert("작성 에러!");
          }
      });
  });

  getRequestsList(1);
  //$("#select-language-from")
});

function utcToLocal (utc) {
  var date = new Date(utc);
  return date;
}

function getRequestsList (t) {
  $.ajax({
    url: '/api/user/requests/stoa?page=1',
    processData: false,
    contentType: false,
    cache: false,
    dataType: "JSON",
    type: 'GET',
    error: function (xhr, ajaxOptions, thrownError) {
      // if (xhr.status == 417) {
      //     alert("올바르지 않은 이메일 주소입니다.");
      // }
      // else if(xhr.status == 412){
      //     alert("이미 존재하는 이메일 주소입니다.");
      // }
      // alert("에러!");
    }
  }).done(function (data) {
    $(data.data).each(function (key, value) {
      $('#tickets').append('<div class="ticket">' +
                           '<div class="ticket-header">' +
                           '<div class="col-lg-4 col-sm-10">' +
                           '<span class="user-name">' + value.request_clientName +'</span>' +
                           '</div>' +
                           '<div class="col-lg-4 none-sm">' +
                           '<span class="user-request">context</span>' +
                           '</div>' +
                           '<div class="col-lg-2 col-sm-10 text-right top-right">' +
                           '<div class="free-translate">무료단문번역</div>' +
                           // '<div class="charge-translate">$ 가격</div>' +
                           '</div>' +
                           '</div>' +
                           '<hr class="none-sm">' +
                           '<div class="ticket-content clearfix">' +
                           '<div class="col-lg-4 col-sm-10">' +
                           '<div class="ticket-profile pull-left">' +
                           '<img class="user-image img-circle" src="/api/access_file/' + value.request_clientPicPath + '">' +
                           '<div class="left-time">' +
                           '<p>남은 시간</p>' +
                           '<p>' + (new Date(value.request_dueTime - (new Date()).getTime())).getMinutes() + '</p>' +
                           '</div>' +
                           '</div>' +
                           '<div class="ticket-information pull-left">' +
                           '<div class="ticket-language-information clearfix">' +
                           '<div class="ticket-language pull-left">' +
                           '<img class="img-circle" src="/static/img/flags/round/' + value.request_originalLang + '.png" alt="country flag">' +
                           '<p class="locale">' + i18n.getI18n('lang_' + value.request_originalLang) + '</p>' +
                           '</div>' +
                           '<div class="ticket-language-direction pull-left">' +
                           '<img class="arrow img-circle" src="/static/img/flags/round/arrow.png" alt="arrow">' +
                           '</div>' +
                           '<div class="ticket-language pull-left">' +
                           '<img class="img-circle" src="/static/img/flags/round/' + value.request_targetLang + '.png" alt="country flag">' +
                           '<p class="locale">' + i18n.getI18n('lang_' + value.request_targetLang) + '</p>' +
                           '</div>' +
                           '</div>' +
                           /***** content type 부분*****/
                           '<!--' +
                           '<div class="ticket-content-type">' +
                           '<img class="img-circle" src="/static/img/flags/round/arrow.png" />' +
                           '<p>출판물</p>' +
                           '</div>' +
                           '<div class="ticket-content-type-blank"></div>' +
                           '<div class="ticket-content-type">' +
                           '<img class="img-circle" src="/static/img/flags/round/arrow.png" />' +
                           '<p">과학</p>' +
                           '</div>' +
                           '-->' +
                           /***** content type 부분*****/
                           '<p class="ticket-word-count">' + value.request_words + ' 단어' + '</p>' +
                           '</div>' +
                           '</div>' +
                           '<div class="col-lg-4 col-sm-10">' +
                           '<div class="ticket-content-text-wrapper">' +
                           '<span class="ticket-content-text">' + value.request_words + '</span>' +
                           '</div>' +
                           '</div>' +
                           '<div class="col-lg-2 col-sm-10 text-right bottom-right">' + (new Date(value.request_registeredTime)).toLocaleString() + '</div>' +
                           '</div>' +
                           '</div>');
    });
  });
}

