$(document).ready(function () {
  i18n.setLanguage('ko');

  $('body').off('selectstart');
  $('body').off('dragstart');
  $('body').off('contextmenu');

  for( i = 0; i < 15; i++){
    $('#select-format').append('<option value="' + i + '" data-imagesrc="/static/img/format/' + i +'.png" data-description="">' + i18n.getI18n('format_' + i) + '</option>');
  }

  for(i = 0; i < 12; i++){
    $('#select-field').append('<option value="' + i + '" data-imagesrc="/static/img/subject/' + i +'.png" data-description="">' + i18n.getI18n('subject_' + i) + '</option>');
  }

  $('.datepicker').datepicker({
    inline: true,
    showOtherMonths: true,
    dateFormat: 'yy-mm-dd',
    onSelect: function (res) {
      var year = res.split('-')[0];
      var month = res.split('-')[1];
      var day = res.split('-')[2];
      $('.time-element[name="timepicker"]').val(res);
    },
    minDate: new Date()
  });

  var $preScrollHeight = 0;

  $('#content-text-area').on('keyup keydown change',function () {
    var $nowScroll = $(document).scrollTop();
    $(this).css('height', 'auto' );
    $(this).height( this.scrollHeight );
    $(document).scrollTop($nowScroll + this.scrollHeight - $preScrollHeight);
    $preScrollHeight = this.scrollHeight;
    $('.character-count').text($(this).val().length + " 글자");
    setPrice();
  });

  $('body').on('input change', '#price-bar', function () {
    $('#now-price').val((parseFloat($(this).val())).toFixed(2));
  });

  $('body').on('input change', '#now-price', function () {
    $('#price-bar').val((parseFloat($(this).val())).toFixed(2));
  });

  $('#select-language-from').ddslick({
    width: 120,
    onSelected: function (selectedData) {
      //callback function: do something with selectedData;
    }
  });

  $('#select-language-to').ddslick({
    width: 120,
    onSelected: function (selectedData) {
      //callback function: do something with selectedData;
    }
  });

  $('#select-format').ddslick({
    height: 300,
    onSelected: function (selectedData) {
      //callback function: do something with selectedData;
    }
  });

  $('#select-field').ddslick({
    height: 300,
    onSelected: function (selectedData) {
      //callback function: do something with selectedData;
    }
  });

  $('body').on('submit', '#request-form', function (e) {
    e.preventDefault();

    var nowDate = new Date();

    $('#request-client-id').val($userInfomation.user_email);
    $('#request-delta-from-due').val(parseInt(($('#deadline-date').datepicker('getDate').getMilliseconds() - nowDate.getMilliseconds()) / 1000));

    var form = $(this)[0];
    var formData = new FormData(form);

    $.ajax({
      url: '/api/requests',
      data: formData,
      processData: false,
      contentType: false,
      dataType: 'JSON',
      type: 'POST',
      success: function (data) {
        alert('의뢰 성공');
        location.href = '/stoa';
      },
      error: function (xhr, ajaxOptions, thrownError) {
        // if (xhr.status == 417) {
        //   alert('올바르지 않은 이메일 주소입니다.');
        // }
        // else if(xhr.status == 412){
        //   alert('이미 존재하는 이메일 주소입니다.');
        // }
        alert('의뢰 실패');
      }
    });
  });

  if (localStorage && localStorage.getItem('content') && localStorage.getItem('content').length) {
    $('#content-text-area').val(localStorage.getItem('content'));
    $('#select-language-from').ddslick('select', { index: localStorage.getItem('originalLang') });
    $('#select-language-to').ddslick('select', { index: localStorage.getItem('targetLang') });
    $('#content-text-area').change();
    localStorage.clear();
  }

  setPrice();
});

function setPrice() {
  if ($('#content-text-area').val().length <= 140) {
    $('.price-min').text('USD 0.00부터~');
    $('#price-bar').attr('min', 0);
  } else if ($('#content-text-area').val().length <= 166) {
    $('.price-min').text('USD 10.00부터~');
    $('#price-bar').attr('min', 10);
  } else {
    $('.price-min').text('USD ' + (parseFloat(($('#content-text-area').val().length * 0.06))).toFixed(2) + "부터~");
    $('#price-bar').attr('min', (parseFloat(($('#content-text-area').val().length * 0.06))).toFixed(2));
  }

  $('#price-bar').attr('max', parseFloat($('#price-bar').attr('min')) * 2 + 99.99);
  $('#now-price').val((parseFloat($('#price-bar').attr('min'))).toFixed(2));
}

