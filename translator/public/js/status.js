$(document).ready(function(){
  i18n.setLanguage('ko');

  $('body').on('click', 'button.close-notification', function () {
    $(this).closest('.notification').remove();
    if (!$.trim($('#notification').html())) {
      $('#notification').remove();
      $('#subject-notification').remove();
    }
  });

  $('body').on('click', '.accept-content', function () {
    showTicket($(this).closest('.ticket').attr('data-id'));

    var value = {
      request_subject: $(this).parent().find('#request_subject').val(),
      request_format: $(this).parent().find('#request_format').val(),
      request_isSOS: $(this).parent().find('#request_isSOS').val(),
      request_id: $(this).parent().find('#request_id').val(),
      request_context: $(this).parent().find('#request_context').val(),
      request_originalLang: $(this).parent().find('#request_originalLang').val(),
      request_targetLang: $(this).parent().find('#request_targetLang').val(),
      request_words: $(this).parent().find('#request_words').val(),
      request_text: $(this).parent().find('#request_text').val()
    };

    var SOSText = '<div class="detail-information">' +
                  '<div class="detail-information-title text-center">포맷</div>' +
                  '<hr />' +
                  '<div class="detail-information-icons clearfix">' +
                  '<div class="detail-information-icon">' +
                  '<img class="img-circle" src="/static/img/subject/' + value.request_subject + '.png" />' +
                  '<p>' + i18n.getI18n('subject_' + value.request_subject) + '</p>' +
                  '</div>' +
                  '<div class="detail-information-arrow"></div>' +
                  '<div class="detail-information-icon">' +
                  '<img class="img-circle" src="/static/img/format/' + value.request_format + '.png" />' +
                  '<p>' + i18n.getI18n('format_' + value.request_format) + '</p>' +
                  '</div>' +
                  '</div>' +
                  '</div>';

    var normalText = '<div class="detail-information">' +
                     '<div class="detail-information-title text-center">포맷</div>' +
                     '<hr />' +
                     '<div class="detail-information-title text-center">무료단문번역</div>' +
                     '</div>';

    var isSOSText = !value.request_isSos ? SOSText : normalText;

    $('body').prepend('<div class="popup" id="popup-' + value.request_id + '">' +
                      '<div class="popup-dim invisible"></div>' +
                      '<div class="detail">' +
                      '<div class="detail-close text-right"><button class="close-popup rm-button-style"><i class="icon ion-ios-close-empty fa-4x"></i></button></div>' +
                      '<div class="detail-header">' +
                      '<span class="pull-left">information</span><span class="pull-right">2015.02.01 15:03</span>' +
                      '</div>' +
                      '<hr />' +
                      '<div class="detail-title">Context</div>' +
                      '<div class="detail-content">' + value.request_context + '</div>' +
                      '<div class="detail-informations clearfix">' +
                      '<div class="detail-information">' +
                      '<div class="detail-information-title text-center">언어</div>' +
                      '<hr />' +
                      '<div class="detail-information-icons clearfix">' +
                      '<div class="detail-information-icon">' +
                      '<img class="img-circle" src="/static/img/flags/round/' + value.request_originalLang + '.png" />' +
                      '<p>' + i18n.getI18n("lang_" + value.request_originalLang) + '</p>' +
                      '</div>' +
                      '<div class="detail-information-arrow">' +
                      '<img class="img-circle" src="/static/img/flags/round/arrow.png" />' +
                      '</div>' +
                      '<div class="detail-information-icon">' +
                      '<img class="img-circle" src="/static/img/flags/round/' + value.request_targetLang + '.png" />' +
                      '<p>' + i18n.getI18n("lang_" + value.request_targetLang) + '</p>' +
                      '</div>' +
                      '</div>' +
                      '</div>' +
                      isSOSText +
                      '<div class="detail-information">' +
                      '<div class="detail-information-title text-center">단어수</div>' +
                      '<hr />' +
                      '<div class="detail-information-words text-center">' + value.request_words + '</div>' +
                      '</div>' +
                      '</div>' +
                      '<div class="detail-title">Original Text</div>' +
                      '<div class="detail-content">' + value.request_text + '</div>' +
                      '</div>' +
                      '</div>');

    setTimeout(
        function () {
          $('#popup-' + value.request_id + ' .invisible').removeClass('invisible');
        },
        1
    );
  });

  $('body').on('click', '.popup button.close-popup', function () {
    $('.popup').remove();
  });

  $('body').on('click', '.remove-content', function () {
    $('body').prepend('<div class="popup" id="popup-' + this.value + '">' +
                      '<div class="popup-dim invisible"></div>' +
                      '<div class="dialog">' +
                      '<div class="dialog-image"><img src="/static/img/9.7.1feedback3_web.png" /></div>' +
                      '<div class="text-center">정말로 삭제하시겠습니까?</div>' +
                      '<div class="dialog-buttons">' +
                      '<button type="button" class="no" data-id="' + this.value + '">아니오</button><button type="button" class="yes" data-id="' + this.value + '">예</button>' +
                      '</div>' +
                      '</div>' +
                      '</div>');

    setTimeout(
        function () {
          $('.popup .invisible').removeClass('invisible');
        },
        1
    );
  });

  $('body').on('click', '.popup .popup-dim', function () {
    var el = this;

    $('.popup .popup-dim, .popup .detail, .popup .dialog').addClass('invisible');

    setTimeout(
        function () {
          $(el).parent().remove();
        },
        400
    );
  });

  $('body').on('click', '.popup .no', function () {
    var targetId = this.getAttribute('data-id');

    $('.popup .popup-dim, .popup .detail, .popup .dialog').addClass('invisible');

    setTimeout(
        function () {
          $('#popup-' + targetId).remove();
        },
        400
    );
  });

  $('body').on('click', '.popup .yes', function () {
    var targetId = this.getAttribute('data-id');

    $.ajax({
      url: '/api/user/requests/incomplete/' + targetId,
      processData: false,
      contentType: false,
      cache : false,
      dataType: "JSON",
      type: 'DELETE',
      error: function (xhr, ajaxOptions, thrownErr7or) {
        // if (xhr.status == 417) {
        //   alert("올바르지 않은 이메일 주소입니다.");
        // }
        // else if(xhr.status == 412){
        //   alert("이미 존재하는 이메일 주소입니다.");
        // }
        // alert("에러!");
      }
    }).done(function (data) {
      alert("삭제되었습니다.");

      $('.popup .popup-dim, .popup .detail, .popup .dialog').addClass('invisible');

      setTimeout(
          function () {
            $('#ticket-' + targetId).remove();
            $('#popup-' + targetId).remove();
          },
          400
      );
    });
  });

  getRequestsList(1);
  ///api/user/requests/incomplete
});

function getRequestsList (t) {
  $.ajax({
    url: '/api/user/requests/incomplete?page=1',
    processData: false,
    contentType: false,
    cache : false,
    dataType: 'JSON',
    type: 'GET',
    error: function (xhr, ajaxOptions, thrownError) {
      // if (xhr.status == 417) {
      //   alert("올바르지 않은 이메일 주소입니다.");
      // }
      // else if(xhr.status == 412){
      //   alert("이미 존재하는 이메일 주소입니다.");
      // }
      // alert("에러!");
    }
  }).done(function (data) {
    $(data.data).each(function (key, value) {
      // isSOS 가 참인데 '무료 단문 번역'인가요?
      // 거짓일때, 무료여야 할 것 같은데..
      // 거짓일때 무료인 코드 바로 아래 주석에 추가합니다.
      // var points = value.request_isSOS ? '<div class="charge-translate"><span>$</span>&nbsp;' + value.request_points + '</div>' : '<div class="free-translate">무료단문번역</div>';
      var points = value.request_isSOS ? '<div class="free-translate">무료단문번역</div>' : '<div class="charge-translate"><span>$</span>&nbsp;' + value.request_points + '</div>';
      var leftTime = (value.request_status >> 0) !== -1 ? '<p>남은 시간</p><p>' + (new Date(value.request_dueTime - (new Date()).getTime())).getMinutes() +'분</p>' : '';

      var hasSubjectText = '<div class="ticket-content-type-information clearfix">' +
                           '<div class="ticket-content-type pull-left" data-id="' + value.request_id + '">' +
                           '<img class="img-circle" src="/static/img/format/' + value.request_format + '.png" />' +
                           '<p>' + i18n.getI18n('format_' + value.request_format) + '</p>' +
                           '</div>' +
                           '<div class="ticket-content-type-blank pull-left"></div>' +
                           '<div class="ticket-content-type pull-left">' +
                           '<img class="img-circle" src="/static/img/subject/' + value.request_subject + '.png" />' +
                           '<p>' + i18n.getI18n('subject_' + value.request_subject) + '</p>' +
                           '</div>' +
                           '</div>';
      var subjectText = !value.request_isSos ? hasSubjectText : "";

      var alertMessage = '<div class="ticket-alert">' +
                         '<div class="ticket-alert-dim"></div>' +
                         '<div class="ticket-alert-inner">' +
                         '<span>히어로가 번역에 착수하기 전에 만료되었습니다.</span>' +
                         '<div class="decide-wrapper">' +
                         '<input type="hidden" id="request_subject" value="' + value.request_subject + '">' +
                         '<input type="hidden" id="request_format" value="' + value.request_format + '">' +
                         '<input type="hidden" id="request_isSOS" value="' + value.request_isSOS + '">' +
                         '<input type="hidden" id="request_id" value="' + value.request_id + '">' +
                         '<input type="hidden" id="request_context" value="' + value.request_context + '">' +
                         '<input type="hidden" id="request_originalLang" value="' + value.request_originalLang + '">' +
                         '<input type="hidden" id="request_targetLang" value="' + value.request_targetLang + '">' +
                         '<input type="hidden" id="request_words" value="' + value.request_words + '">' +
                         '<input type="hidden" id="request_text" value="' + value.request_text + '">' +
                         '<button type="button" class="accept-content">내용 확인 / 재등록</button>' +
                         '<button type="button" class="remove-content" value="' + value.request_id + '">삭제하기</button>' +
                         '</div>' +
                         '</div>' +
                         '</div>';

      var setAlert = (value.request_status >> 0) !== -1 ? alertMessage : '';

      $('#tickets').append('<div class="ticket" id="ticket-' + value.request_id + '" data-id="' + value.request_id + '">' +
                           setAlert +
                           '<div class="ticket-header">' +
                           '<div class="col-lg-4 col-sm-10">' +
                           '<span class="user-name">' + value.request_clientName + '</span>' +
                           '</div>' +
                           '<div class="col-lg-4 none-sm">' +
                           '<span class="user-request">context</span>' +
                           '</div>' +
                           '<div class="col-lg-2 col-sm-10 text-right top-right">' +
                           points +
                           '</div>' +
                           '</div>' +
                           '<hr class="none-sm"/>' +
                           '<div class="ticket-content clearfix">' +
                           '<div class="col-lg-4 col-sm-10">' +
                           '<div class="ticket-profile pull-left">' +
                           '<img class=""user-image img-circle src="/api/access_file/' + value.request_clientPicPath + '"/>' +
                           '<div class="left-time">' +
                           leftTime +
                           '</div>' +
                           '</div>' +
                           '<div class="ticket-information pull-left">' +
                           '<div class="ticket-language-information clearfix">' +
                           '<div class="ticket-language pull-left">' +
                           '<img class="img-circle" src="/static/img/flags/round/' + value.request_originalLang + '.png" />' +
                           '<p class="locale">' + i18n.getI18n("lang_" + value.request_originalLang) + '</p>' +
                           '</div>' +
                           '<div class="ticket-language-direction pull-left">' +
                           '<img class="arrow img-circle" src="/static/img/flags/round/arrow.png" />' +
                           '</div>' +
                           '<div class="ticket-language pull-left">' +
                           '<img class="img-circle" src="/static/img/flags/round/' + value.request_targetLang + '.png" />' +
                           '<p class="locale" i18n="lang_' + value.request_targetLang +'"></p>' +
                           '</div>' +
                           '</div>' +
                           subjectText +
                           '<p class="ticket-word-count">' + value.request_words + ' 단어</p>' +
                           '</div>' +
                           '</div>' +
                           '<div class="col-lg-4 col-sm-10">' +
                           '<div class="ticket-content-text-wrapper">' +
                           '<span class="ticket-content-text">' + value.request_text + '</span>' +
                           '</div>' +
                           '</div>' +
                           '<div class="col-lg-2 col-sm-10 text-right bottom-right">' + (new Date(value.request_registeredTime)).toLocaleString() + '</div>' +
                           '</div>' +
                           '</div>');
    });
  });
}
