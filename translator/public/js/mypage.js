$(document).ready(function(){
  i18n.setLanguage('ko');

  /* TEMP
  $(".mypage-image-img").attr('src', '/api/access_file/' + $userInfomation.user_profilePicPath);
  $(".mypage-email").text($userInfomation.user_email);
  $(".mypage-detail-language").text(i18n.getI18n("lang_"+$userInfomation.user_motherLang) );
  $(".mypage-detail-point").text($userInfomation.user_point);
  */
  // $(".mypage-detail-language").text(i18n.getI18n("lang_1"));
  // alert(i18n.getI18n("lang_0"));

  $('body').on('click', '.mypage-image-edit', function () {
    $('#user-picture').click();
  });

  $('body').on('submit', '#form-user-profile', function (e) {
    e.preventDefault();

    /*
    var form = this;
    var formData = new FormData(form);

    $.ajax( {
      url: '/api/user/profile',
      data: formData,
      processData: false,
      contentType: false,
      dataType: "JSON",
      type: 'POST',
      error: function (xhr, ajaxOptions, thrownErr7or) {
        if (xhr.status == 417) {
          alert("올바르지 않은 이메일 주소입니다.");
        } else if(xhr.status == 412){
          alert("이미 존재하는 이메일 주소입니다.");
        }
        alert("에러!");
      }
    }).done(function(data){
      alert("정상적으로 변경되었습니다.");
    });
    */
  });

  $('body').on('click', '.close-cropper, .popup-dim', function () {
    $('.popup').remove();
  });

  $('body').on('click', '#change-password', function () {
    if ($(window).outerWidth() <= 860) {
      $('.change-password-wrapper').stop().slideToggle();
    } else {
      $('.change-password-wrapper').stop().toggle();
    }
  });

  $('body').on('change', '#user-picture', function (e) {
    if (this.files && this.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        var $croppedImg;
        var $tt;

        $("body").prepend('<div class="popup">' +
                          '<div class="popup-dim"></div>' +
                          '<div class="img-setting">' +
                          '<div class="img-setting-close text-right">' +
                          '<button class="close-cropper rm-button-style"><i class="icon ion-ios-close-empty fa-4x"></i></button>' +
                          '</div>' +
                          '<div class="img-setting-header">' +
                          '<span class="img-setting-header-left pull-left">프로필 사진 지정</span>' +
                          '</div>' +
                          '<div class="img-setting-content">' +
                          '<div class="img-setting-crop-div">' +
                          '<form id="user-image-crop-form" method="POST" enctype="multipart/form-data">' +
                          '<img class="img-setting-crop crop-image" />' +
                          '<div class="crop-guide"><i class="fa fa-arrows" aria-hidden="true"></i>위치를 조정하고 확대할 수 있습니다.</div>' +
                          '<input class="rm-button-style" type="submit" value="저장">' +
                          '</form>' +
                          '</div>' +
                          '</div>' +
                          '</div>' +
                          '</div>');

        $('.img-setting-crop').attr('src', e.target.result);
        $('.img-setting-crop').cropbox({ width: 300, height: 300, showControls: 'auto' }).on('cropbox', function(e, results, img) {
          $croppedImg = img.getBlob();
          $tt = img.getDataURL();
        });

        $('body').on('submit', '#user-image-crop-form', function (e) {
          e.preventDefault();

          var formData = new FormData();

          formData.append("user-picture", $croppedImg, "thumb.png");

          $.ajax({
            url: '/api/user/profile',
            data: formData,
            processData: false,
            contentType: false,
            dataType: "JSON",
            type: 'POST',
            error: function (xhr, ajaxOptions, thrownErr7or) {
              // if (xhr.status == 417) {
              //   alert("올바르지 않은 이메일 주소입니다.");
              // }
              // else if(xhr.status == 412){
              //   alert("이미 존재하는 이메일 주소입니다.");
              // }
              // alert("에러!");
            }
          }).done(function (data) {
            alert("정상적으로 변경되었습니다.");
            location.reload();
          });
        });
        // $("#user-picture-form").submit();
      }
      reader.readAsDataURL(this.files[0]);
    }
  });
});

