requirejs.config({
  baseUrl: '/static/js/lib',
  paths: {
    app: '../app'
  }
});

requirejs(['jquery', 'app/AuthService', 'app/TranslatingEditorController', 'app/GnbView', 'app/SideNavView'], function ($, authService, translatingEditorController, GnbView, SideNavView) {
	authService.setUserProfile(null);
	authService.getUserProfile({
		success: function (data) {
			var htUserProfile = data.htUserProfile;

			if(htUserProfile.user_isTranslator === false) {
				location.href='/';
			}

			var oGnbView = new GnbView($('.topbar'));
			var oSideNavView = new SideNavView($("#page-container"));

			new translatingEditorController($("#page-container > .content-main > .container.js-translating"));
		},
		error: function () {
			location.href='/about';
		}
	});
});
