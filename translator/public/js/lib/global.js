var $userInformation;

// $(window).on('popstate',function (e) {
//   if(!$('#header-profile').hasClass('invisible')) {
//     history.forward();
//     setTimeout(function() {
//       $('#header-profile').addClass('invisible');
//     }, 100);
//   } else {
//     location.reload();
//   }
// });

$(window).on('popstate', function (e) {
  if (!$('#header-profile').hasClass('invisible')) {
    $('#header-profile').addClass('invisible');
  }
});

$.ajax({
  url: '/api/user/profile',
  processData: false,
  contentType: false,
  cache : false,
  async: false,
  dataType: 'JSON',
  type: 'GET',
  error: function () {
    // TEMP
    // location.href = "/about";
  }
}).done(function (data) {
  $userInformation = data;
});

$('#header').load('header.html', function () {
  $('body').on('selectstart', function (e) { return false; });
  $('body').on('dragstart', function (e) { return false; });
  $('body').on('contextmenu', function() { return false; });
  $('body').prepend('<div class="popup invisible" id="popup"><div class="popup-dim"></div></div>');

  $(document).bind('mousedown', function (e) {
    if (e.target.id != 'img-header' && !$('#header-profile').find(e.target).length) {
      if (!$('#header-profile').hasClass('invisible')) {
        $('#header-profile').addClass('invisible');
        history.back();
      }
    }
  });

  $('a[href|="' + $(location).attr('pathname') + '"]').addClass('selected');

  $('#img-header').click(function () {
    if ($('#header-profile').hasClass('invisible')) {
      // BEFORE
      // $("#header-profile-container").css("max-height",$(window).height() - 150);
      // AFTER
      $('#header-profile-container').css('max-height', $(window).outerHeight() - 90 + 'px');
      $('#header-profile').removeClass('invisible');
      var obj = { Page: "CICERON", Url: location.href };
      history.pushState(obj, obj.Page, obj.Url);
    } else {
      $('#header-profile').addClass('invisible');
      history.back();
    }
  });

  $('body').on('click', '#btn-logout', function () {
    $.ajax({
      url: '/api/logout',
      processData: false,
      contentType: false,
      cache : false,
      dataType: 'JSON',
      type: 'GET',
      error: function () {
        alert('잠시 후 시도해주세요.');
      }
    }).done(function (data) {
      location.href = '/about';
    });
  });

  $('#footer').load('footer.html', function () {
    // TEMP
    $userInformation = $userInformation || { user_profilePicPath: '' };

    $('#img-header').attr('src', '/api/access_file/' + $userInformation.user_profilePicPath);
    $('#img-profile').attr('src', '/api/access_file/' + $userInformation.user_profilePicPath);
    $('#header-profile-name').text($userInformation.user_name);

    $.ajax({
      url: '/api/notification',
      processData: false,
      contentType: false,
      cache : false,
      dataType: 'JSON',
      type: 'GET',
      error: function(){
        //location.href = "/about";
      }
    }).done(function (data) {
      if (data.numberOfNoti > 0 && $(location).attr('pathname') !== '/status') {
        $('#btnStatus').append('<i class="fa fa-circle" style="color: #FE8B97; font-size: 5px; position: absolute;"></i>');
        $('#btnStatus').addClass("shake-slow shake-constant shake-constant--hover");
      }

      $('body').on('click', 'a', function (e) {
        // e = e || window.event;
        // e.preventDefault();
        // var obj = { Page: 'CICERON', Url: $(this).attr('href') };
        // history.pushState(obj, obj.Page, obj.Url);
        // location.replace(obj.Url);
        // $.ajax({ url: obj.Url }).done(function (data) {
        //   $('html').html(data);
        // });

        // $('html').empty();
        // $('html').append('<iframe style="width: 100%; height: 1000px; border: none;"></iframe>')
        // $('iframe').attr('src', obj.Url);
      });
    });
  });
});

var scrollTimer = null;
var functionScroll = function (e) {
  var $nowScrollTop = $(this).scrollTop();
  if ($nowScrollTop > 10) {
    $('#header').addClass('scrolled');
  } else {
    $('#header').removeClass('scrolled');
  }
};

$(this).scroll(function () {
  if (scrollTimer) {
    clearTimeout(scrollTimer);   // clear any previous pending timer
  }
  scrollTimer = setTimeout(functionScroll, 5);   // set new timer
});

$('body').on('touchmove', function () {
  if (scrollTimer) {
    clearTimeout(scrollTimer);   // clear any previous pending timer
  }
  scrollTimer = setTimeout(functionScroll, 5);   // set new timer
});

functionScroll();

$('head').append('<link rel="stylesheet" href="css/csshake.min.css">');

function showTicket ($id) {
  $.ajax({
    url: '/api/user/requests/' + $id,
    processData: false,
    contentType: false,
    cache : false,
    async: true,
    dataType: 'JSON',
    type: 'GET',
    error: function() {
      // location.href = "/about";
    }
  }).done(function (data) {
    // alert(data.data[0].request_id);
    $(data.data).each(function(i, item) {
      var requestSOSText = '<div class="detail-infomation">' +
                           '<span class="detail-infomation-title">포맷</span><hr />' +
                           '<div class="detail-infomation-icons">' +
                           '<div class="detail-infomation-icon">' +
                           '<img src="/static/img/format/' + item.request_format + '.png" style="height: 30px; border-radius: 50%;" />' +
                           '<p style="font-size: 8px; padding: 0px; width: 30px;">' + i18n.getI18n('format_' + item.request_format) + '</p>' +
                           '</div>' +
                           '<div style="float: left; width: 10px; padding-top: 20px;"></div>' +
                           '<div class="detail-infomation-icon">' +
                           '<img src="/static/img/subject/' + item.request_subject + '.png" style="height: 30px; border-radius: 50%;" />' +
                           '<p style="font-size: 8px; padding: 0px; width: 30px;">' + i18n.getI18n('subject_' + item.request_subject) + '</p>' +
                           '</div>' +
                           '</div>' +
                           '</div>';
      var requestNormalText = '<div class="detail-infomation">' +
                              '<span class="detail-infomation-title">포맷</span> <hr />' +
                              '<span class="detail-infomation-title">무료단문번역</span>' +
                              '</div>';

      var requestIsSOSText = item.request_isSos ? requestSOSText : requestNormalText;

      $('body').prepend('<div class="popup invisible" id="popup-' + item.request_id + '">' +
                        '<div class="popup-dim"></div>' +
                        '<div class="detail">' +
                        '<div class="detail-close">' +
                        '<i class="icon ion-ios-close-empty fa-4x"></i>' +
                        '</div>' +
                        '<div class="detail-header"' +
                        '<span class="pull-left"> Infomation </span>' +
                        '<span class="pull-right"> 2015.02.01 15:03 </span>' +
                        '</div>' +
                        '<hr />' +
                        '<span class="detail-title">Context</span><span class="detail-content">' + item.request_context + '</span>' +
                        '<div class="detail-infomations">' +
                        '<div class="detail-infomation">' +
                        '<span class="detail-infomation-title">언어</span><hr />' +
                        '<div class="detail-infomation-icons">' +
                        '<div class="detail-infomation-icon">' +
                        '<img src="/static/img/flags/round/' + item.request_originalLang + '.png" style="height: 30px; border-radius: 50%;" />' +
                        '<p style="font-size: 8px; padding: 0px; width: 30px;">' + i18n.getI18n("lang_" + item.request_originalLang) + '</p>' +
                        '</div>' +
                        '<div style="float: left; width: 10px; padding-top: 20px;"><img src="/static/img/flags/round/arrow.png" style="width: 10px; border-radius: 50%;" /></div>' +
                        '<div class="detail-infomation-icon">' +
                        '<img src="/static/img/flags/round/'+item.request_targetLang+'.png" style="height: 30px; border-radius: 50%;" />' +
                        '<p style="font-size: 8px; padding: 0px; width: 30px;">' + i18n.getI18n("lang-" + item.request_targetLang) + '</p>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        requestIsSOSText +
                        '<div class="detail-infomation">' +
                        '<span class="detail-infomation-title">단어수</span> <hr />' +
                        '<span class="detail-infomation-words">' + item.request_words + '</span>' +
                        '</div>' +
                        '</div>' +
                        '<span class="detail-title">Original Text</span><span class="detail-content">' + item.request_text + '</span>' +
                        '</div>' +
                        '</div>');

      $('#popup-' + item.request_id).delay(1).queue(function () {
        $('#popup-' + item.request_id).removeClass('invisible');
      });

      $('#popup-' + item.request_id).find('.detail-close i').click(function () {
        $('#popup-' + item.request_id).remove();
      });
    });
  });
}
