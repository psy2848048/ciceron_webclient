$(document).ready(function(){
  i18n.setLanguage('ko');

  var isEdit = false;

  $.ajax({
    url: '/api/user/requests/complete/groups',
    processData: false,
    contentType: false,
    cache : false,
    dataType: "JSON",
    type: 'GET',
    error: function(xhr, ajaxOptions, thrownError){
      // if (xhr.status == 417) {
      //   alert('올바르지 않은 이메일 주소입니다.');
      // }
      // else if(xhr.status == 412){
      //   alert('이미 존재하는 이메일 주소입니다.');
      // }
      // alert('에러!');
    }
  }).done(function (data) {
    $((data.data).reverse()).each(function(key, value) {
      $('#folder').prepend('<div class="folder" value="' + value.id + '">' +
                           '<span class="folder-title">' + value.name + '</span>' +
                           '<span class="folder-delete" value="' + value.id + '"><i class="icon ion-ios-minus-outline fa-2x"></i></span>' +
                           '</div>');
    });

    $('.folder').on('contextmenu', function(){
      $('.folder-edit').click();
    });

    $('.folder').click(function (e) {
      // alert($(this).attr('value'));
      if (isEdit) {
        if (e.target.getAttribute('class') && (e.target.getAttribute('class').match(/folder\-delete/) || e.target.getAttribute('class').match(/ion\-ios\-minus\-outline/))) {
          return;
        }

        if ($(this).find('.folder-title')[0].nodeName.match(/input/i)) {
          return;
        }

        var attrs = { };
        $.each($(this).find('.folder-title')[0].attributes, function (idx, attr) {
          attrs[attr.nodeName] = attr.nodeValue;
        });

        attrs['type'] = 'text';
        attrs['value'] = $(this).find('.folder-title').text();

        $(this).find('.folder-title').replaceWith(function () {
          return $('<input />', attrs).append($(this).contents());
        });

        $(this).find('.folder-title').select();
        $(this).find('input.folder-title').on('blur', function(){
          $(this).off();
          var what = $(this);
          var formData = new FormData();
          formData.append('group_name', $(this).val())
          $.ajax({
            url: '/api/user/requests/complete/groups/' + $(this).parent().attr('value'),
            processData: false,
            data: formData,
            contentType: false,
            cache : false,
            dataType: "JSON",
            type: 'PUT',
            error: function(xhr, ajaxOptions, thrownError){
              // if (xhr.status == 417) {
              //   alert('올바르지 않은 이메일 주소입니다.');
              // }
              // else if(xhr.status == 412){
              //   alert('이미 존재하는 이메일 주소입니다.');
              // }
              alert('에러가 발생하였습니다.');
              location.reload();
            }
          }).done(function(data){
            what.text(what.val());
            $.each(what[0].attributes, function(idx, attr) {
              attrs[attr.nodeName] = attr.nodeValue;
            });
            what.replaceWith(function () {
              return $('<span />', attrs).append(what.contents());
            });
            //location.reload();
          });
        });

        $(this).find('input.folder-title').on('keyup', function(e){
          if (e.which === 13 || e.keyCode === 13) {
            $(this).blur();
          }
        });
      } else {
        // temp
      }
    });

    $('.folder-delete').click(function () {
      $('body').prepend('<div class="popup invisible" id="popup' + $(this).attr('value') + '">' +
                        '<div class="popup-dim"></div>' +
                        '<div class="dialog">' +
                        '<div class="dialog_image">' +
                        '<img src="/static/img/9.7.1feedback3_web.png" />' +
                        '</div>' +
                        '<span class="dialog_message">정말로 삭제하시겠습니까?<br />해당 폴더 내의 번역물들은 기본 폴더로 이동됩니다.</span>' +
                        '<div class="dialog_buttons">' +
                        '<button type="button" class="positive">아니오</button>' +
                        '<button type="button" class="negative" value="'+$(this).attr("value")+'">예</button>' +
                        '</div>' +
                        '</div>' +
                        '</div>');
      $("#popup"+$(this).attr("value")).delay(1).queue(function(){$(this).removeClass("invisible");});
      $("#popup"+$(this).attr("value")).find(".negative").click(function(){
        $.ajax({
          url: '/api/user/requests/complete/groups/' + $(this).attr("value"),
          processData: false,
          contentType: false,
          cache : false,
          dataType: "JSON",
          type: 'DELETE',
          error: function(xhr, ajaxOptions, thrownError){
            // if (xhr.status == 417) {
            //   alert("올바르지 않은 이메일 주소입니다.");
            // }
            // else if(xhr.status == 412){
            //   alert("이미 존재하는 이메일 주소입니다.");
            // }
            alert("에러!");
          }
        }).done(function(data){
          location.reload();
        });
      });
      $("#popup"+$(this).attr("value")).find(".positive").click(function(){
        $(".popup").remove();
      });
    });
  });

  $('.folder_add').click(function(){
    $("body").prepend('<div class="popup invisible" id="popup"><div class="popup_background"></div><div class="dialog"><form class="frmAddFolder" method="POST"> <!--<div class="dialog_image"><img src="/static/img/9.7.1feedback3_web.png" /></div>--><span class="dialog_message">새 폴더 이름을 지정해주세요.</span><input type="text" name="group_name" class="dialog_inputText" /><div class="dialog_buttons"><button type="submit" class="positive">만들기</button><button type="button" class="negative">취소</button></div></form></div></div>'); 
    $("#popup").delay(1).queue(function(){$("#popup").removeClass("invisible");});
    $(".frmAddFolder").submit(function(e){
      e.preventDefault();
      var form = $(this)[0];
      var formData = new FormData(form);
      $.ajax({
        url: '/api/user/requests/complete/groups',
        data: formData,
        processData: false,
        contentType: false,
        dataType: "JSON",
        type: 'POST',
        success: function(data){
          location.reload();
        },
        error: function(xhr, ajaxOptions, thrownError){
          if (xhr.status == 401) {
            alert(xhr.message);
          }
          else{
            alert("알 수 없는 에러로 폴더 만들기 실패하였습니다.");
          }
        }
      });
    });
    $(".negative").click(function(){
      $("#popup").remove();
    });
  });
  $('.folder_edit').click(function(){
    if($('.folder').hasClass('edit')){
      $('.folder').removeClass('edit');
      isEdit = false;
      $(this).find('.folder_title').html('<i class="ion-compose"></i>');
    }
    else{
      $('.folder').addClass('edit');
      isEdit = true;
      $(this).find('.folder_title').html('변경 완료');
    }
  });
  $('.completed_contentTextWrap').click(function(){
    if($(this).css('height') != '80px'){
      $(this).css('height','80px');
    }
    else{
      $('.completed_contentTextWrap').css('height','80px');
      $(this).css('height','auto');
    }
  });
});

