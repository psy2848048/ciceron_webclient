/**
 * About PageController
 * ( 본문 편집 Component )
 *
 * @class se.editor.canvas.view.Paragraph
 * @author happyhj@gmail.com
 * @since 2015.05.15
 *
 */
// requirejs.config({
//     baseUrl: '/static/js/lib',
//     paths: {
//         app: '../app'
//     }
// });

// //requirejs(['jquery', 'app/domain', 'app/AuthService', 'app/ApplyHeroView', 'app/I18nResolver'], function($, domain, authService, ApplyHeroView, I18nResolver) {
// requirejs(['jquery', 'sha256'], function($, Sha256) {
	
	//var ApplyHeroView = ApplyHeroView;
	//var i18n = I18nResolver.getMap();
var $logosWidth, $slideWidth;
var $SlideLeft = 0;

///////////////////////////////////////////////////////////////////////////////////////////////
$(window).resize(function(e){
    $logosWidth = $("#logos").find("img").length * 300;
    $("#logos").width($logosWidth);
    
    $slideWidth = $(".slide").length * $("#login").width();
    $("#slide").width($slideWidth);
    $(".slide").width($("#login").width());
    
    
    $SlideLeft = parseInt($SlideLeft / $("#login").width()) * $("#login").width();
    $("#slide").css({
        transform: "translate3d(-" + $SlideLeft +"px, 0, 0)"
    });
});


$(document).ready(function(){
    $("#openmenu").text("");
    $.ajax({
        url: '/api/user/profile',
        processData: false,
        contentType: false,
        cache : false,
        dataType: 'JSON',
        type: 'GET',
        error: function(){
            $("#openmenu").text("LOGIN");
        }
    }).done(function(data){
        $userInfomation = data;
        $("#openmenu").html("<img src='/api/access_file/"+$userInfomation.user_profilePicPath+"' class='img-circle' style='overflow: visible; width: 36px; height: 36px; margin-top:-6px;'/>");
        $("#img-header").attr('src', '/api/access_file/' + $userInfomation.user_profilePicPath);
        $("#img-profile").attr('src', '/api/access_file/' + $userInfomation.user_profilePicPath);
        $("#header-profile-name").text($userInfomation.user_name);
        
        
        $("#profile").addClass("loggedIn");
        $(".frmLogin").addClass("loggedIn");
        
        $("#btn_logout").click(function(){
    
        $.ajax({
                url: '/api/logout',
                processData: false,
                contentType: false,
                cache : false,
                dataType: 'JSON',
                type: 'GET',
                error: function(){
                    alert("잠시 후 시도해주세요.");
                }
            }).done(function(data){
                location.href = "/about";
            });
            
        });
        
        
    });
    
    
    
    //$(".fadeInLeft").css("opacity",0);
    //$(".section3_box_right").css("opacity",0);
    //$(".section3_box_left").css("opacity",0);
    $('html,body').animate({scrollTop: 0}, 0);
    $(window).resize();
    
    
    //$("#slide-left").animate({left: -$("#slide-left").width(), opacity: 0},200);
    //$("#slide-right").animate({right: -$("#slide-right").width(), opacity: 0},200);
    
    $("#slide-left").css({
        transform: "translate3d(-"+$("#slide-left").width() +"px, 0, 0)",
        opacity: 0
    });
    $("#slide-right").css({
        transform: "translate3d("+$("#slide-right").width() +"px, 0, 0)",
        opacity: 0
    });
    
    $(".amountUp").text("0");
    
    var functionAutoScrollTimer = setInterval(function () {
        $("#logos").css({
            transform: "translate3d(-" + $logosLeft +"px, 0, 0)"
        });
        
        if($logosLeft > $logosWidth - $("#partners").width()){
            $logosLeft = -$("#partners").width() / 4;
        }
        $logosLeft += $("#partners").width() / 4;
        
        //clearInterval(functionAutoScrollTimer);
    }, 3000);
    
    var functionSlideTimer = setInterval(function(){
        $("#slide-right").click();            
    }, 7000);
    
    
    $("#slide-left").click(function(e){
        //$("#login").width()
        $SlideLeft = parseInt($SlideLeft / $("#login").width()) * $("#login").width();
        
        $SlideLeft-=$("#login").width();
        if($SlideLeft<0){
            $SlideLeft=$slideWidth - $("#login").width();
        }
        $("#slide").css({
            transform: "translate3d(-" + $SlideLeft +"px, 0, 0)"
        });
        clearInterval(functionSlideTimer);
        functionSlideTimer = setInterval(function(){
            $("#slide-right").click();            
        }, 7000);
    });
    
    $("#slide-right").click(function(e){
        //$("#login").width()
        $SlideLeft = parseInt($SlideLeft / $("#login").width()) * $("#login").width();
        
        $SlideLeft+=$("#login").width();
        if($SlideLeft>=$slideWidth){
            $SlideLeft=0;
        }
        $("#slide").css({
            transform: "translate3d(-" + $SlideLeft +"px, 0, 0)"
        });
        clearInterval(functionSlideTimer);
        functionSlideTimer = setInterval(function(){
            $("#slide-right").click();            
        }, 7000);
    });
    
    
    /*
    if($nowScrollTop>=$('#login').height()){
        $("#header_background").css("opacity",0.7);
        
    }    
    else if($nowScrollTop>$('#login').height()-$('#login').height()/2 && $nowScrollTop<=$('#login').height()){
        $("#header_background").css("opacity",($('#login').height()/2-$('#login').height()+$nowScrollTop)/($('#login').height()/2)*0.7);
    }
    else{
        $("#header_background").css("opacity","0");
    }
    */
    
    var isHeaderShort = false;
        
    $("#learn_more").click(function(e){
        e.preventDefault();
        var targetOffset = $("#section1").offset().top;
        $('html,body').animate({scrollTop: targetOffset}, 500);
    });
    
    $("#btn_top").click(function(e){
        $('html,body').animate({scrollTop: 0}, 500);
        
        
    });
    
    var $logosLeft = 0;
    var $isSection3Animated = false;
    var scrollTimer = null;
    var functionScroll = function(e){
        /*
        if($nowScrollTop>$('#login').height()){
            $("#header_background").stop(true, true).animate({"opacity": 0.7}, 500);
            
        }    
        else if($nowScrollTop>$('#login').height()-$('#login').height()/2 && $nowScrollTop<=$('#login').height()){
            $("#header_background").stop(true, true).animate({"opacity": ($('#login').height()/2-$('#login').height()+$nowScrollTop)/($('#login').height()/2)*0.7}, 500);
        }
        else{
            $("#header_background").stop(true, true).animate({"opacity": "0"}, 500);
        }
        */

        var $nowScrollTop = $(this).scrollTop();
        
        if($(this).scrollTop()>10){
            if(!isHeaderShort){
                isHeaderShort = true;
                /*
                $("#header_background").stop(false, false).animate({"opacity": 0.7}, 600);
                $("#header_left").find("img").stop(false, false).animate({"height": "30px"}, 100);
                $("#header_bottom").stop(false, false).animate({"top": "40px"}, 100);
                */
                $("#header").addClass("scrolled");
                $("#header_background").addClass("scrolled");
                $("#header_left").addClass("scrolled");
                $("#header_bottom").addClass("scrolled");
            }
        } 
        else{
            if(isHeaderShort){
                isHeaderShort = false;
                /*
                $("#header_background").stop(false, false).animate({"opacity": "0"}, 600);
                $("#header_left").find("img").stop(false, false).animate({"height": "40px"}, 100);
                $("#header_bottom").stop(false, false).animate({"top": "60px"}, 100);
                */
                $("#header").removeClass("scrolled");
                $("#header_background").removeClass("scrolled");
                $("#header_left").removeClass("scrolled");
                $("#header_bottom").removeClass("scrolled");
            }
        }
        
        
        if($nowScrollTop>$('#section5').offset().top - $(window).height() / 1.5 && $("#href_section5").css('color')!='#02C1CF') {
            
            $("#header_bottom").find("a").css('color','#FFFFFF');
            $("#href_section5").css('color','#02C1CF');
        }
        else if($nowScrollTop>$('#section4').offset().top - $(window).height() / 1.5 && $("#href_section4").css('color')!='#02C1CF'){
            
            $("#header_bottom").find("a").css('color','#FFFFFF');
            $("#href_section4").css('color','#02C1CF');
        }
        else if($nowScrollTop>$('#section3').offset().top - $(window).height() / 1.5 && $("#href_section3").css('color')!='#02C1CF'){
            
            $("#header_bottom").find("a").css('color','#FFFFFF');
            $("#href_section3").css('color','#02C1CF');
            if(!$isSection3Animated){
                $isSection3Animated = true;
                /*
                var $nowDelay = 0;
                $(".section3_box_right").css("margin-left","100px");
                $(".section3_box_right").animate({opacity: 1, marginLeft: 0},600,"swing");
                $(".section3_box_left").css("margin-left","100px");
                $(".section3_box_left").animate({opacity: 1, marginLeft: 0},600,"swing");
                
                
                $(".section3_box_right").each(function(e){
                    $(this).css("margin-left","100px");
                    $(this).delay($nowDelay).animate({opacity: 1, marginLeft: 0},600,"swing");
                    $nowDelay+=200;
                });
                $(".section3_box_left").each(function(e){
                    $(this).css("margin-left","100px");
                    $(this).delay($nowDelay).animate({opacity: 1, marginLeft: 0},600,"swing");
                    $nowDelay+=200;
                });
                */
                
            }
        }
        else if($nowScrollTop>$('#section2').offset().top - $(window).height() / 1.5 && $("#href_section2").css('color')!='#02C1CF'){
            
            $("#header_bottom").find("a").css('color','#FFFFFF');
            $("#href_section2").css('color','#02C1CF');
        }
        else if($nowScrollTop>$('#section1').offset().top - $(window).height() / 1.5 && $("#href_section1").css('color')!='#02C1CF'){
            
            $("#header_bottom").find("a").css('color','#FFFFFF');
            $("#href_section1").css('color','#02C1CF');
        }
        else{
            $("#header_bottom").find("a").css('color','#FFFFFF');
        }
        
        $(".fadeInLeft").each(function(){
            if($nowScrollTop>$(this).offset().top - $(window).height() / 1.5){
                $(this).removeClass("fadeInLeft");
                $(this).addClass("fadeInLeftEnded");
                //var $origin = $(this).css("margin-right");
                //$(this).css("margin-right", "500px");
                //$(this).animate({opacity: 1, marginRight: $origin},500,"swing");
            }
        });
        
        //amount_up
        
        
        $(".amountUp").each(function(){
            if($nowScrollTop>$(this).offset().top - $(window).height() / 1.5){
                $(this).removeClass("amountUp");
                $(this).addClass("amountUpEnded");
                var $now = 0;
                var $to = $(this).attr('to');
                var $how = parseInt($to / 100);
                var $what = $(this);
                var timer = setInterval(function(){
                    $now+=$how;
                    
                    if($now>=$to){
                        clearInterval(timer);
                        $now = $to;
                    }
                    $what.text($now);
                }, 9);
                //var $origin = $(this).css("margin-right");
                //$(this).css("margin-right", "500px");
                //$(this).animate({opacity: 1, marginRight: $origin},500,"swing");
            }
        });
        
    };
    
    
    $(this).scroll(function(){
        if (scrollTimer) {
            clearTimeout(scrollTimer);   // clear any previous pending timer
        }
        scrollTimer = setTimeout(functionScroll, 5);   // set new timer
    });
    
    $('body').on('touchmove', function () {
        if (scrollTimer) {
            clearTimeout(scrollTimer);   // clear any previous pending timer
        }
        scrollTimer = setTimeout(functionScroll, 5);   // set new timer
    });
    
    functionScroll();
    
    $("#header_left").find("img").click(function(e){
        e.preventDefault();
        var targetOffset = 0;
        $('html,body').stop().animate({scrollTop: targetOffset}, 500,"swing");
    });
    
    $("#header_bottom").find("a").click(function(e){
        e.preventDefault();
        var targetOffset = $($(this).attr('href')).offset().top-50;
        $('html,body').stop().animate({scrollTop: targetOffset}, 500,"swing");
    });
    
    $("#openmenu").click(function(e){
        $("#popup_background").css('visibility','visible');
        $("#popup_background").css('opacity','0');
        $("#popup_background").stop(false, false).animate({opacity: "0.5"},300);
        
        $("#ham_menu").css('right', -$("#ham_menu").width());
        $("#ham_menu").css('visibility','visible');
        $("#ham_menu").stop(false, false).animate({right: "0px"},300);
    });
    
    $("#popup_background").click(function(e){
        $("#ham_menu").stop(false, false).animate({right: -$("#ham_menu").width()},300);
        
        //$("#popup").find("div").stop(false, false).animate({opacity: "0"}, 300, "swing",  
        $("#popup_background").stop(false, false).animate({opacity: "0"}, 300, "swing",  
        function(){
            $(this).css('visibility','hidden');
            $(this).css('opacity', '1');
        });
        //$("#popup_background").animate({opacity: "0"},500).css('visibility','hidden');
    });
    
    $("#login").hover(
        function(e){
            
            $("#slide-left").css({
                transform: "translate3d(0, 0, 0)",
                opacity: 0.8
            });
            $("#slide-right").css({
                transform: "translate3d(0, 0, 0)",
                opacity: 0.8
            });
            
            //$("#slide-left").stop().animate({left: 0, opacity: 0.5},200);
            //$("#slide-right").stop().animate({right: 0, opacity: 0.5},200);
        },
        function(e){
            
            
            $("#slide-left").css({
                transform: "translate3d(-"+$("#slide-left").width() +"px, 0, 0)",
                opacity: 0
            });
            $("#slide-right").css({
                transform: "translate3d("+$("#slide-right").width() +"px, 0, 0)",
                opacity: 0
            });
            
            
            //$("#slide-left").stop().animate({left: -$("#slide-left").width(), opacity: 0},200);
            //$("#slide-right").stop().animate({right: -$("#slide-right").width(), opacity: 0},200);
        }
    );
    
    
    
    
    $("#frmLogin").submit(function(e){
        
        e.preventDefault();
        var token = "";
        $.ajax({
            url: '/api/login',
            processData: false,
            contentType: false,
            dataType: "JSON",
            type: 'GET',
            success: function(data){
                
                token = data.identifier;
                $("#login_hidden_password").val(Sha256.hash(token + Sha256.hash($("#login_password").val()).toString() + token).toString());
                
                var form = $("#frmLogin")[0];
                var formData = new FormData(form);
                
                $.ajax({
                    url: '/api/login',
                    data: formData,
                    processData: false,
                    contentType: false,
                    dataType: "JSON",
                    type: 'POST',
                    success: function(data){
                        location.href = "/stoa";
                    },
                    error: function(xhr, ajaxOptions, thrownError){
                        alert("로그인 실패");
                    }
                });
            }
        });
        
    });
    
    // $("#frmLogin").find("input").on("keyup", function(e){
    //     if(e.which == 13){
    //         $("#frmLogin").submit();
    //     }
    // });
    
    
    $("#frmSignUp").submit(function(e){
        e.preventDefault();
        
        if($("#signup_password1").val() != $("#signup_password2").val()){
            alert("패스워드가 일치하지 않습니다.");
            return;
        }
        $("#signup_hidden_password").val(Sha256.hash($("#signup_password1").val()).toString());
                
        var form = $(this)[0];
        var formData = new FormData(form);
        
        $.ajax({
            url: '/api/signup',
            data: formData,
            processData: false,
            contentType: false,
            dataType: "JSON",
            type: 'POST',
            success: function(data){
                alert("회원가입 성공");
                location.href = "/about";
            },
            error: function(xhr, ajaxOptions, thrownError){
                if (xhr.status == 417) {
                    alert("올바르지 않은 이메일 주소입니다.");
                }
                else if(xhr.status == 412){
                    alert("이미 존재하는 이메일 주소입니다.");
                }
            }
        });
        
    });
    
    $("#frmSignUp").find("input").keypress(function(e){
        if(e.which == 13){
            $("#frmSignUp").submit();
        }
    });
    
});
        
    
    ///////////////////////////////////////////////////////////////////////////////////////////////
// });
