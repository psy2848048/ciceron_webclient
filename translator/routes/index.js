var express = require('express');
var router = express.Router();

// 스토아 / 장바구니 / 작업중인 번역 / 작업내역 메뉴

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

// 스토아
router.get('/stoa', function (req, res, next) {
  res.render('stoa', { title: 'STOA' });
});

router.get('/cart', function (req, res, next) {
  res.render('cart', { title: 'STOA' });
});

router.get('/condition', function (req, res, next) {
  res.render('condition', { title: 'STOA' });
});

// 작업 내역
router.get('/list', function (req, res, next) {
  res.render('list', { title: 'STOA' });
});

router.get('/profile', function (req, res, next) {
  res.render('profile', { title: 'STOA' });
});

router.get('/notification', function (req, res, next) {
  res.render('notification', { title: 'STOA' });
});

router.get('/mypage', function (req, res, next) {
  res.render('mypage', { title: 'STOA' });
});

router.get('/status', function (req, res, next) {
  res.render('status', { title: 'STOA' });
});

// 작업 중인 번역
router.get('/condition/form', function (req, res, next) {
  res.render('condition_form', { title: 'STOA' });
});

module.exports = router;

