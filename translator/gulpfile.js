var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

gulp.task('sass', function () {
  return gulp.src('public/css/application.scss')
    .pipe(sass())
    .pipe(gulp.dest('public/css'))
    .pipe(cleanCSS({ compatibility: 'ie8' })) 
    .pipe(rename({ suffix: '.min' })) 
    .pipe(gulp.dest('public/css'));
});

gulp.task('sass_all', function () {
  return gulp.src('public/css/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('public/css/min'));
});

gulp.task('script', function () {
  return gulp.src('public/js/jaewon/*.js')
    .pipe(gulp.dest('public/js/jaewon/min'))
    .pipe(uglify())
    .pipe(rename({ suffix: '.min' })) 
    .pipe(gulp.dest('public/js/jaewon/min'));
});

gulp.task('watch', function () {
  gulp.watch('public/js/jaewon/*.js', ['script']);
  gulp.watch('public/css/*.scss', ['sass']);
});

gulp.task('default', ['sass', 'script', 'watch']);

