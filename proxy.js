var http = require('http'),
	httpProxy = require('http-proxy'),
	connect = require('connect'),
	morgan = require('morgan');

var proxy = httpProxy.createProxyServer({});
var logger = morgan('dev')

var app = connect()
.use(logger)
.use(requestHandler);

http.createServer(app).listen(80);

function isApiUrl(url) {
	return url.substring(0, 4) === "/api";
}

function requestHandler(req, res) {
   if (isApiUrl(req.url)) {
       var ip = (req.headers['x-forwarded-for'] || '').split(',')[0] 
                   || req.connection.remoteAddress;
       req.headers['x-forwarded-for-client-ip'] = ip;
       console.log('API REQUEST');
       console.log(ip);
       proxy.web(req, res, {
           target: 'http://localhost:5000'
       }); 
   } else {
       console.log('STATIC REQUEST');
       proxy.web(req, res, {
           target: 'http://localhost:15000'
       }); 
   }   
   proxy.on('error', function(e) {
       console.log(e);    
   }); 
}


